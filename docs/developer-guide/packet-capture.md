## Android

1. Root your phone.
2. Install termux and tcpdump.
3. Run: `tcpdump -i any -p -s 0 -w /sdcard/capture.pcap`.
4. Capture packets.
5. Run: `Ctrl + C` to stop capturing.
6. Copy the capture file to your computer.

## iOS

1. Connect your iPhone to your Mac.
2. Find out your iPhone's UUID through Xcode (Xcode -> Window -> Devices and Simulators).
3. Run: `rvictl -s <UUID>`.
4. Start Wireshark and select the rvi0 interface.
5. Capture packets.
6. Run: `rvictl -x <UUID>` to disconnect rvi0 interface.
7. Disconnect your iPhone from your Mac.
