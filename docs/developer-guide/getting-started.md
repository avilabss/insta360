## Setting up your development environment

1. Make sure [poetry](https://python-poetry.org/docs/#installation) is installed.
2. Run: `poetry config virtualenvs.in-project true`
3. Ensure you are using supported python version (3.10 and above): `poetry env use 3.10`
4. Run: `poetry install`

## Contribution Guidelines

1. Write tests for each new feature: integration and unit tests with 90%+ coverage
2. Google style docstrings should be used.
