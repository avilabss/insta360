## Protobuf

The messages exchanged between the Android app and the camera use Protocol Buffers, which is an open standard by Google. To decode the messages, you need the specific language description files (\*.proto files) which can be extracted from the compiled binary file `libOne.so` from the official Insta360 app by decompiling it.

## Sent Packets

### Sync Packet

| Offset |     Content      | Bytes | Description                  |
| :----: | :--------------: | :---: | :--------------------------- |
|   0    |  Packet Length   |   4   | Overall length of the packet |
|   4    | `0x06 0x00 0x00` |   3   | Message Type: Sync Packet    |
|   7    |    syNceNdinS    |  10   | Magic String                 |

### Keep Alive Packet

| Offset |     Content      | Bytes | Description                  |
| :----: | :--------------: | :---: | :--------------------------- |
|   0    |  Packet Length   |   4   | Overall length of the packet |
|   4    | `0x05 0x00 0x00` |   3   | Message Type: Keep Alive     |

### Phone Commands

| Offset |     Content      |  Bytes   | Description                                                                                                                 |
| :----: | :--------------: | :------: | :-------------------------------------------------------------------------------------------------------------------------- |
|   0    |  Packet Length   |    4     | Overall length of the packet                                                                                                |
|   4    | `0x04 0x00 0x00` |    3     | Message Type: Phone Command                                                                                                 |
|   7    |   Message Code   |    2     | PHONE_COMMAND_GET_OPTIONS, PHONE_COMMAND_TAKE_PICTURE, etc                                                                  |
|   9    |      `0x02`      |    1     | Unknown                                                                                                                     |
|   10   | Sequence Number  |    3     | Each command sent to the camera has an incrementing sequence number. The camera will respond with the same sequence number. |
|   13   | `0x80 0x00 0x00` |    3     | Unknown                                                                                                                     |
|   16   | Protobuf Message | Variable | Protobuf encoded message                                                                                                    |

## Received Packets

### Notifications or Response to Phone Commands

| Offset |     Content      |  Bytes   | Description                                              |
| :----: | :--------------: | :------: | :------------------------------------------------------- |
|   0    |  Packet Length   |    4     | Overall length of the packet                             |
|   4    | `0x04 0x00 0x00` |    3     | Response Type: Phone Command                             |
|   7    |  Response Code   |    2     | 200: OK, CAMERA_NOTIFICATION_CURRENT_CAPTURE_STATUS, etc |
|   9    |      `0x02`      |    1     | Unknown                                                  |
|   10   | Sequence Number  |    3     | Same as the sequence number sent in the phone command    |
|   13   |      `0x80`      |    1     | Unknown                                                  |
|   14   |     Unknown      |    2     | Unknown                                                  |
|   16   | Protobuf Message | Variable | Protobuf encoded message                                 |
