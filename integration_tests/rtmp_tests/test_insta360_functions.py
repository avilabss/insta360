import logging
import os
import signal
import mimetypes
import time

import unittest
import tempfile
from unittest.mock import patch, MagicMock

from insta360.rtmp import Client, CommandFailedException


logging.basicConfig(level=logging.ERROR)
logger = logging.getLogger(__name__)


def handle_message(_msg):
    pass


@patch("insta360.rtmp.rtmp.protobuf_to_dict")
class TestInsta360Functions(unittest.TestCase):
    def setUp(self) -> None:
        self.tmp_dir = tempfile.TemporaryDirectory()
        self.raw_file = os.path.join(os.getcwd(), "output", "dump.raw")
        self.mp4_file = os.path.join(self.tmp_dir.name, "dump.mp4")
        self.camera = Client(
            callback=handle_message, logger=logger, output_filepath=self.mp4_file
        )
        self.camera.logger.error = MagicMock()
        self.camera.open()
        return super().setUp()

    def tearDown(self) -> None:
        self.camera.close()
        self.tmp_dir.cleanup()

        if os.path.exists(self.raw_file):
            os.remove(self.raw_file)

        return super().tearDown()

    def test_get_camera_info(self, mock_protobuf_to_dict):
        from insta360.pb2 import get_options_pb2

        seq = self.camera.get_camera_info()
        self.camera._check_if_command_successful(seq)
        mock_protobuf_to_dict.assert_called_once()
        call_args = mock_protobuf_to_dict.call_args
        self.assertIsInstance(call_args[0][0], get_options_pb2.GetOptionsResp)
        assert call_args[1] == {
            "message_code": self.camera.PHONE_COMMAND_GET_OPTIONS,
            "response_code": self.camera.RESPONSE_CODE_OK,
        }

    def test_get_camera_files_list(self, mock_protobuf_to_dict):
        from insta360.pb2 import get_file_list_pb2

        seq = self.camera.get_camera_files_list()
        self.camera._check_if_command_successful(seq)
        mock_protobuf_to_dict.assert_called_once()
        call_args = mock_protobuf_to_dict.call_args
        self.assertIsInstance(call_args[0][0], get_file_list_pb2.GetFileListResp)
        assert call_args[1] == {
            "message_code": self.camera.PHONE_COMMAND_GET_FILE_LIST,
            "response_code": self.camera.RESPONSE_CODE_OK,
        }

    def test_start_stop_capture(self, mock_protobuf_to_dict):
        from insta360.pb2 import stop_capture_pb2

        seq = self.camera.start_capture()
        self.camera._check_if_command_successful(seq)
        time.sleep(2)
        seq = self.camera.stop_capture()
        self.camera._check_if_command_successful(seq)

        mock_protobuf_to_dict.assert_called()
        call_args = mock_protobuf_to_dict.call_args_list[-1]
        self.assertIsInstance(call_args[0][0], stop_capture_pb2.StopCaptureResp)
        assert call_args[1] == {
            "message_code": self.camera.PHONE_COMMAND_STOP_CAPTURE,
            "response_code": self.camera.RESPONSE_CODE_OK,
        }

    def test_take_picture(self, mock_protobuf_to_dict):
        from insta360.pb2 import take_picture_pb2

        seq = self.camera.take_picture()
        self.camera._check_if_command_successful(seq)
        mock_protobuf_to_dict.assert_called()
        call_args = mock_protobuf_to_dict.call_args_list[-1]
        self.assertIsInstance(call_args[0][0], take_picture_pb2.TakePictureResponse)
        assert call_args[1] == {
            "message_code": self.camera.PHONE_COMMAND_TAKE_PICTURE,
            "response_code": self.camera.RESPONSE_CODE_OK,
        }

    def test_get_normal_video_options(self, mock_protobuf_to_dict):
        from insta360.pb2 import get_photography_options_pb2

        seq = self.camera.get_normal_video_options()
        self.camera._check_if_command_successful(seq)
        mock_protobuf_to_dict.assert_called_once()
        call_args = mock_protobuf_to_dict.call_args
        self.assertIsInstance(
            call_args[0][0], get_photography_options_pb2.GetPhotographyOptionsResp
        )
        assert call_args[1] == {
            "message_code": self.camera.PHONE_COMMAND_GET_PHOTOGRAPHY_OPTIONS,
            "response_code": self.camera.RESPONSE_CODE_OK,
        }

    def test_get_capture_current_status(self, mock_protobuf_to_dict):
        from insta360.pb2 import get_current_capture_status_pb2

        seq = self.camera.get_capture_current_status()
        self.camera._check_if_command_successful(seq)
        mock_protobuf_to_dict.assert_called_once()
        call_args = mock_protobuf_to_dict.call_args
        self.assertIsInstance(
            call_args[0][0], get_current_capture_status_pb2.GetCurrentCaptureStatusResp
        )
        assert call_args[1] == {
            "message_code": self.camera.PHONE_COMMAND_GET_CURRENT_CAPTURE_STATUS,
            "response_code": self.camera.RESPONSE_CODE_OK,
        }

    def test_set_normal_video_options(self, _mock_protobuf_to_dict):
        seq = self.camera.set_normal_video_options(
            record_resolution="RES_2560_1280P30",
            fov_type="FOV_WIDE",
            focal_length_value=0.1,
            gamma_mode="STANDARD",
            white_balance="WB_2700K",
            white_balance_value=1,
            function_mode="FUNCTION_MODE_NORMAL_VIDEO",
        )
        self.camera._check_if_command_successful(seq)

    def test_start_stop_live_stream(self, _mock_protobuf_to_dict):
        seq = self.camera.start_live_stream()
        self.camera._check_if_command_successful(seq)
        time.sleep(5)
        seq = self.camera.stop_live_stream()
        self.camera._check_if_command_successful(seq)

        raw_file_exists = os.path.exists(self.raw_file)
        self.assertTrue(raw_file_exists)

        mp4_file_exists = os.path.exists(self.mp4_file)
        self.assertTrue(mp4_file_exists)

        file_type = mimetypes.guess_type(self.mp4_file)[0]
        self.assertEqual(file_type, "video/mp4")

    def test_start_stop_preview(self, _mock_protobuf_to_dict):
        seq = self.camera.start_preview_stream()
        self.camera._check_if_command_successful(seq)
        time.sleep(5)
        seq = self.camera.stop_preview_stream()
        self.camera._check_if_command_successful(seq)

        raw_file_exists = os.path.exists(self.raw_file)
        self.assertTrue(raw_file_exists)

        mp4_file_exists = os.path.exists(self.mp4_file)
        self.assertTrue(mp4_file_exists)

        file_type = mimetypes.guess_type(self.mp4_file)[0]
        self.assertEqual(file_type, "video/mp4")

    @patch("insta360.rtmp.Client.close")
    def test_signal_handler(self, mock_close, _mock_protobuf_to_dict):
        assert self.camera.program_killed is False
        pid = os.getpid()
        with self.assertRaises(SystemExit):
            os.kill(pid, signal.SIGTERM)
        assert self.camera.program_killed is True
        mock_close.assert_called()

    def test_check_if_command_successful(self, _mock_protobuf_to_dict):
        self.camera.sent_messages_codes = [1]
        with self.assertRaises(CommandFailedException):
            self.camera._check_if_command_successful(1, wait_for_seconds=1)

    def test_keep_alive(self, _mock_protobuf_to_dict):
        time.sleep(self.camera.KEEPALIVE_INTERVAL_SEC + 0.5)
        assert self.camera.is_connected is True

    def test_keep_alive_after_disconnected(self, _mock_protobuf_to_dict):
        self.camera.is_connected = False
        self.camera.RECONNECT_TIMEOUT_SEC = 2
        time.sleep(self.camera.RECONNECT_TIMEOUT_SEC + 5)
        assert self.camera.is_connected is True

    def test_get_camera_type(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.get_camera_type()

    def test_get_serial_number(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.get_serial_number()

    def test_get_exposure_settings(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.get_exposure_settings()

    def test_set_exposure_settings(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.set_exposure_settings()

    def test_set_capture_settings(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.set_capture_settings()

    def test_get_capture_settings(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.get_capture_settings()

    def test_get_camera_uuid(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.get_camera_uuid()

    def test_set_time_lapse_option(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.set_time_lapse_option()

    def test_start_time_lapse(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.start_time_lapse()

    def test_stop_time_lapse(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.stop_time_lapse()

    def test_is_camera_connected(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.is_camera_connected()

    def test_get_battery_status(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.get_battery_status()

    def test_get_storage_state(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.get_storage_state()
