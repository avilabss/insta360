import os
import unittest
import tempfile
import time
import subprocess
import mimetypes


class TestInsta360CMD(unittest.TestCase):
    def setUp(self) -> None:
        self.tmp_dir = tempfile.TemporaryDirectory()
        self.raw_file = os.path.join(os.getcwd(), "output", "dump.raw")
        self.mp4_file = os.path.join(self.tmp_dir.name, "dump.mp4")
        return super().setUp()

    def tearDown(self) -> None:
        self.tmp_dir.cleanup()

        if os.path.exists(self.raw_file):
            os.remove(self.raw_file)

        return super().tearDown()

    def test_invalid_command(
        self,
    ):
        cmd = ["python3", "./insta360_cmd.py", "-c", "invalid"]
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        time.sleep(10)
        process.terminate()
        time.sleep(10)

        self.assertEqual(process.returncode, 2)

    def test_capture(
        self,
    ):
        cmd = ["python3", "./insta360_cmd.py", "-c", "capture"]
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        time.sleep(10)
        process.terminate()
        time.sleep(10)

    def test_preview(
        self,
    ):
        cmd = ["python3", "./insta360_cmd.py", "-c", "preview", "-o", self.mp4_file]
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        time.sleep(10)
        process.terminate()
        time.sleep(10)

        raw_file_exists = os.path.exists(self.raw_file)
        self.assertTrue(raw_file_exists)

        mp4_file_exists = os.path.exists(self.mp4_file)
        self.assertTrue(mp4_file_exists)

        file_type = mimetypes.guess_type(self.mp4_file)[0]
        self.assertEqual(file_type, "video/mp4")

    def test_livestream(
        self,
    ):
        cmd = ["python3", "./insta360_cmd.py", "-c", "livestream", "-o", self.mp4_file]
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        time.sleep(10)
        process.terminate()
        time.sleep(10)

        raw_file_exists = os.path.exists(self.raw_file)
        self.assertTrue(raw_file_exists)

        mp4_file_exists = os.path.exists(self.mp4_file)
        self.assertTrue(mp4_file_exists)

        file_type = mimetypes.guess_type(self.mp4_file)[0]
        self.assertEqual(file_type, "video/mp4")

    def test_capture_and_livestream(
        self,
    ):
        cmd = [
            "python3",
            "./insta360_cmd.py",
            "-c",
            "capture",
            "-c",
            "livestream",
            "-o",
            self.mp4_file,
        ]
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        time.sleep(10)
        process.terminate()
        time.sleep(10)

        raw_file_exists = os.path.exists(self.raw_file)
        self.assertTrue(raw_file_exists)

        mp4_file_exists = os.path.exists(self.mp4_file)
        self.assertTrue(mp4_file_exists)

        file_type = mimetypes.guess_type(self.mp4_file)[0]
        self.assertEqual(file_type, "video/mp4")

    def test_capture_and_preview_stream(
        self,
    ):
        cmd = [
            "python3",
            "./insta360_cmd.py",
            "-c",
            "capture",
            "-c",
            "preview",
            "-o",
            self.mp4_file,
        ]
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        time.sleep(10)
        process.terminate()
        time.sleep(10)

        raw_file_exists = os.path.exists(self.raw_file)
        self.assertTrue(raw_file_exists)

        mp4_file_exists = os.path.exists(self.mp4_file)
        self.assertTrue(mp4_file_exists)

        file_type = mimetypes.guess_type(self.mp4_file)[0]
        self.assertEqual(file_type, "video/mp4")
