import unittest
import os
import time

from insta360.rtmp import Client as RTMPClient
from insta360.osc import *


class TestClient(unittest.TestCase):
    def setUp(self) -> None:
        self.tmp_dir = os.path.join(os.getcwd(), "tmp")

        if not os.path.exists(self.tmp_dir):
            os.makedirs(self.tmp_dir)

        # TODO: Implement and use osc to capture a video
        self.cam = RTMPClient()
        self.osc_client = Client()

        self.cam.open()

        self.cam.start_capture()
        time.sleep(5)
        self.cam.stop_capture()
        time.sleep(2)

        return super().setUp()

    def tearDown(self) -> None:
        self.cam.close()

        return super().tearDown()

    def test_list_files_without_thumbnail(self):
        files = self.osc_client.list_files(entry_count=10)
        self.assertIsInstance(files, ListFilesResponse)
        self.assertEqual(files.state, "done")
        self.assertEqual(len(files.results.entries), 10)
        self.assertEqual(files.results.entries[0].thumbnail.base64, None)
        self.assertEqual(files.results.entries[0].thumbnail.content, None)

    def test_list_files_with_thumbnail(self):
        files = self.osc_client.list_files(entry_count=1, max_thumb_size=100)
        self.assertIsInstance(files, ListFilesResponse)
        self.assertEqual(files.state, "done")
        self.assertEqual(len(files.results.entries), 1)
        self.assertIsInstance(files.results.entries[0].thumbnail, Thumbnail)
        self.assertIsNot(files.results.entries[0].thumbnail.base64, "")
        self.assertIsInstance(files.results.entries[0].thumbnail.content, bytes)

    def test_download_file(self):
        files = self.osc_client.list_files(entry_count=1)
        self.assertIsInstance(files, ListFilesResponse)
        self.assertEqual(files.state, "done")

        file = files.results.entries[0]
        file_name = file.name
        file_url = file.fileUrl
        save_path = os.path.join(self.tmp_dir, file_name)

        result = self.osc_client.download_file(file_url, save_path)
        self.assertTrue(result)

        file_exists = os.path.exists(save_path)
        self.assertTrue(file_exists)

    def test_delete_files(self):
        files = self.osc_client.list_files(entry_count=1)
        self.assertIsInstance(files, ListFilesResponse)
        self.assertEqual(files.state, "done")

        file = files.results.entries[0]
        file_url = file.fileUrl

        result = self.osc_client.delete_files([file_url])
        self.assertIsInstance(result, DeleteFilesResponse)
        self.assertEqual(result.state, "done")
