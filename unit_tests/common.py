# Mock Classes
class MockResponse:
    def __init__(self, body: dict, status_code: int):
        self.body = body
        self.status_code = status_code

    def json(self):
        return self.body


def ensure_camera_connected(connect_host: str):
    return MockResponse({}, 200)
