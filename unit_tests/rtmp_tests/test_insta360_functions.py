import time

import unittest
from unittest.mock import patch

from insta360.rtmp import Client, CommandFailedException
from unit_tests.common import ensure_camera_connected


@patch("insta360.rtmp.Client._send_message")
class TestInsta360Functions(unittest.TestCase):
    @patch(
        "insta360.rtmp.rtmp.ensure_camera_connected",
        side_effect=ensure_camera_connected,
    )
    def setUp(self, _mock_ensure_camera_connected) -> None:
        self.camera = Client()
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    def test_sync_local_time_to_camera(self, mock_send_message):
        timestamp = int(time.time())
        seconds_from_gmt = 120
        self.camera.sync_local_time_to_camera(
            timestamp=timestamp, seconds_from_gmt=seconds_from_gmt
        )
        mock_send_message.assert_called_with(
            {
                "optionTypes": ["LOCAL_TIME", "TIME_ZONE"],
                "value": {
                    "local_time": timestamp,
                    "time_zone_seconds_from_GMT": seconds_from_gmt,
                },
            },
            self.camera.PHONE_COMMAND_SET_OPTIONS,
        )

    def test_get_camera_info(self, mock_send_message):
        self.camera.get_camera_info()
        mock_send_message.assert_called_with(
            {
                "optionTypes": [
                    "BATTERY_STATUS",
                    "SERIAL_NUMBER",
                    "UUID",
                    "STORAGE_STATE",
                    "FIRMWAREREVISION",
                    "CAMERA_TYPE",
                    "TEMP_VALUE",
                    "CAMERA_POSTURE",
                    "OPTIONS_NUM",
                ]
            },
            self.camera.PHONE_COMMAND_GET_OPTIONS,
        )

    def test_take_picture(self, mock_send_message):
        self.camera.take_picture()
        mock_send_message.assert_called_with(
            {"mode": "NORMAL"}, self.camera.PHONE_COMMAND_TAKE_PICTURE
        )

    def test_get_camera_files_list(self, mock_send_message):
        self.camera.get_camera_files_list()
        mock_send_message.assert_called_with(
            {"media_type": "VIDEO_AND_PHOTO", "limit": 500},
            self.camera.PHONE_COMMAND_GET_FILE_LIST,
        )

    def test_set_normal_video_options(self, mock_send_message):
        self.camera.set_normal_video_options(
            record_resolution="RES_2560_1280P30",
            fov_type="FOV_ULTRAWIDE",
            focal_length_value=17.4,
            gamma_mode="VIVID",
            white_balance="WB_2700K",
            white_balance_value=5.0,
            function_mode="FUNCTION_MODE_LIVE_STREAM",
        )
        mock_send_message.assert_called_with(
            {
                "optionTypes": [
                    "RECORD_RESOLUTION",
                    "FOV_TYPE",
                    "FOCAL_LENGTH_VALUE",
                    "VIDEO_GAMMA_MODE",
                    "WHITE_BALANCE",
                    "WHITE_BALANCE_VALUE",
                ],
                "value": {
                    "record_resolution": "RES_2560_1280P30",
                    "fov_type": "FOV_ULTRAWIDE",
                    "focal_length_value": 17.4,
                    "gamma_mode": "VIVID",
                    "white_balance": "WB_2700K",
                    "white_balance_value": 5.0,
                },
                "function_mode": "FUNCTION_MODE_LIVE_STREAM",
            },
            self.camera.PHONE_COMMAND_SET_PHOTOGRAPHY_OPTIONS,
        )

    def test_get_normal_video_options(self, mock_send_message):
        self.camera.get_normal_video_options()
        mock_send_message.assert_called_with(
            {
                "option_types": [
                    "EXPOSURE_BIAS",
                    "WHITE_BALANCE",
                    "WHITE_BALANCE_VALUE",
                    "VIDEO_GAMMA_MODE",
                    "VIDEO_EXPOSURE_OPTIONS",
                    "VIDEO_ISO_TOP_LIMIT",
                    "RECORD_RESOLUTION",
                    "FOV_TYPE",
                    "FOCAL_LENGTH_VALUE",
                ],
                "function_mode": "FUNCTION_MODE_NORMAL_VIDEO",
            },
            self.camera.PHONE_COMMAND_GET_PHOTOGRAPHY_OPTIONS,
        )

    def test_start_capture(self, mock_send_message):
        self.camera.start_capture()
        mock_send_message.assert_called_with(
            {"mode": "Capture_MODE_NORMAL"}, self.camera.PHONE_COMMAND_START_CAPTURE
        )

    def test_stop_capture(self, mock_send_message):
        self.camera.stop_capture()
        mock_send_message.assert_called_with({}, self.camera.PHONE_COMMAND_STOP_CAPTURE)

    def test_start_live_stream(self, mock_send_message):
        self.camera.start_live_stream()

        mock_send_message.assert_any_call(
            {
                "optionTypes": ["RECORD_RESOLUTION"],
                "value": {"record_resolution": "RES_2560_1280P30"},
                "function_mode": "FUNCTION_MODE_LIVE_STREAM",
            },
            self.camera.PHONE_COMMAND_SET_PHOTOGRAPHY_OPTIONS,
        )

        mock_send_message.assert_any_call(
            {
                "enableVideo": True,
                "videoBitrate": 40,
                "resolution": "RES_2560_1280P30",
                "enableGyro": True,
                "videoBitrate1": 40,
                "resolution1": "RES_480_240P30",
                "isForLive": True,
            },
            self.camera.PHONE_COMMAND_START_LIVE_STREAM,
        )

    def test_start_preview_stream(self, mock_send_message):
        self.camera.start_preview_stream()

        mock_send_message.assert_any_call(
            {
                "enableVideo": True,
                "audioSampleRate": 48000,
                "enableGyro": True,
                "resolution": "RES_1440_720P30",
                "resolution1": "RES_424_240P15",
            },
            self.camera.PHONE_COMMAND_START_LIVE_STREAM,
        )

    @patch("insta360.rtmp.Client._check_if_command_successful")
    def test_start_live_stream_set_video_options_failed(
        self, mock_check_if_command_successful, mock_send_message
    ):
        mock_check_if_command_successful.side_effect = CommandFailedException()

        with self.assertRaises(CommandFailedException):
            self.camera.start_live_stream()

        mock_send_message.assert_any_call(
            {
                "optionTypes": ["RECORD_RESOLUTION"],
                "value": {"record_resolution": "RES_2560_1280P30"},
                "function_mode": "FUNCTION_MODE_LIVE_STREAM",
            },
            self.camera.PHONE_COMMAND_SET_PHOTOGRAPHY_OPTIONS,
        )

        mock_send_message.assert_any_call(
            {
                "enableVideo": True,
                "videoBitrate": 40,
                "resolution": "RES_2560_1280P30",
                "enableGyro": True,
                "videoBitrate1": 40,
                "resolution1": "RES_480_240P30",
                "isForLive": True,
            },
            self.camera.PHONE_COMMAND_START_LIVE_STREAM,
        )

    @patch("insta360.rtmp.Client._check_if_command_successful")
    def test_stop_live_stream_command_failed(
        self, mock_check_if_command_successful, mock_send_message
    ):
        mock_check_if_command_successful.side_effect = CommandFailedException()
        self.camera.stop_live_stream()
        mock_send_message.assert_called_with(
            {}, self.camera.PHONE_COMMAND_STOP_LIVE_STREAM
        )

    def test_stop_live_stream(self, mock_send_message):
        self.camera.stop_live_stream()
        mock_send_message.assert_called_with(
            {}, self.camera.PHONE_COMMAND_STOP_LIVE_STREAM
        )

    @patch("insta360.rtmp.Client._check_if_command_successful")
    def test_stop_preview_stream_command_failed(
        self, mock_check_if_command_successful, mock_send_message
    ):
        mock_check_if_command_successful.side_effect = CommandFailedException()
        self.camera.stop_preview_stream()
        mock_send_message.assert_called_with(
            {}, self.camera.PHONE_COMMAND_STOP_LIVE_STREAM
        )

    def test_stop_preview_stream(self, mock_send_message):
        self.camera.stop_preview_stream()
        mock_send_message.assert_called_with(
            {}, self.camera.PHONE_COMMAND_STOP_LIVE_STREAM
        )

    def test_get_capture_current_status(self, mock_send_message):
        self.camera.get_capture_current_status()
        mock_send_message.assert_called_with(
            {}, self.camera.PHONE_COMMAND_GET_CURRENT_CAPTURE_STATUS
        )

    def test_get_camera_type(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.get_camera_type()

    def test_get_serial_number(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.get_serial_number()

    def test_get_exposure_settings(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.get_exposure_settings()

    def test_set_exposure_settings(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.set_exposure_settings()

    def test_set_capture_settings(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.set_capture_settings()

    def test_get_capture_settings(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.get_capture_settings()

    def test_get_camera_uuid(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.get_camera_uuid()

    def test_set_time_lapse_option(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.set_time_lapse_option()

    def test_start_time_lapse(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.start_time_lapse()

    def test_stop_time_lapse(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.stop_time_lapse()

    def test_is_camera_connected(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.is_camera_connected()

    def test_get_battery_status(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.get_battery_status()

    def test_get_storage_state(self, _mock_send_message):
        with self.assertRaises(NotImplementedError):
            self.camera.get_storage_state()
