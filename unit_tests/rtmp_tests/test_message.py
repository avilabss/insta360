import time
import unittest
from unittest.mock import MagicMock, patch

from insta360.rtmp import Client
from insta360.pb2.set_options_pb2 import SetOptions
from unit_tests.common import ensure_camera_connected


message = {
    "optionTypes": ["LOCAL_TIME", "TIME_ZONE"],
    "value": {"local_time": int(time.time()), "time_zone_seconds_from_GMT": 0},
}

message_bytes = b"\n\x02\x0c\r\x12\x06`\xe1\xe3\x83\xb1\x06"


def handle_message(_msg):
    pass


@patch("insta360.rtmp.Client._send_packet")
@patch("insta360.rtmp.rtmp.protobuf_to_dict")
class TestCameraMessage(unittest.TestCase):
    @patch(
        "insta360.rtmp.rtmp.ensure_camera_connected",
        side_effect=ensure_camera_connected,
    )
    def setUp(self, _mock_ensure_camera_connected) -> None:
        self.camera = Client(callback=handle_message)
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    def test_send_message(self, mock_protobuf_to_dict, mock_send_packet):
        with patch(
            "insta360.rtmp.threading.Lock", return_value=MagicMock()
        ) as mock_lock:
            self.camera.socket_lock = mock_lock

            curr_msg_seq = self.camera.message_seq

            result = self.camera._send_message(
                message, self.camera.PHONE_COMMAND_SET_OPTIONS
            )
            assert self.camera.message_seq == curr_msg_seq + 1
            assert (
                self.camera.sent_messages_codes[curr_msg_seq]
                == self.camera.PHONE_COMMAND_SET_OPTIONS
            )
            mock_send_packet.assert_called_once()
            assert result == curr_msg_seq
            assert curr_msg_seq in self.camera.sent_messages_codes
            mock_protobuf_to_dict.assert_not_called()

    def test_send_message_failed(self, mock_protobuf_to_dict, mock_send_packet):
        mock_send_packet.side_effect = Exception()

        with patch(
            "insta360.rtmp.threading.Lock", return_value=MagicMock()
        ) as mock_lock:
            self.camera.socket_lock = mock_lock
            curr_msg_seq = self.camera.message_seq

            result = self.camera._send_message(
                message, self.camera.PHONE_COMMAND_SET_OPTIONS
            )
            assert self.camera.message_seq == curr_msg_seq + 1
            mock_send_packet.assert_called_once()
            assert result == curr_msg_seq
            assert curr_msg_seq not in self.camera.sent_messages_codes
            mock_protobuf_to_dict.assert_not_called()

    def test_parse_protobuf_message(self, mock_protobuf_to_dict, _mock_send_packet):
        res = self.camera._parse_protobuf_message(SetOptions(), message_bytes)
        assert res.option_types == [12, 13]
        assert res.value.local_time == 1713435105
        mock_protobuf_to_dict.assert_not_called()

    def test_parse_protobuf_message_failed(
        self, mock_protobuf_to_dict, _mock_send_packet
    ):
        res = self.camera._parse_protobuf_message(SetOptions(), b"abcde")
        assert res is None
        mock_protobuf_to_dict.assert_not_called()

    def test_parse_packet(self, mock_protobuf_to_dict, _mock_send_packet):
        assert self.camera._parse_packet(b"") is None
        assert self.camera._parse_packet(self.camera.PKT_KEEPALIVE) is None

        self.camera.is_connected = False
        self.camera._parse_packet(self.camera.PKT_SYNC)
        assert self.camera.is_connected is True
        mock_protobuf_to_dict.assert_not_called()

    def test_parse_packet_error_response(
        self, mock_protobuf_to_dict, _mock_send_packet
    ):
        seq = 1001
        pkt_data = (
            b"\x04\x00\x00"  # response_type
            b"\xf4\x01"  # response_code
            b"\x02"  # unknown_1
            b"\xe9\x03\x00"  # response_seq  (1001)
            b"\xe9\x03\x00"  # unknowns
            b"\x08\x03\x12\x06Failed"  # Body
        )
        self.camera.sent_messages_codes[seq] = self.camera.PHONE_COMMAND_SET_OPTIONS
        self.camera._parse_packet(pkt_data)
        assert seq not in self.camera.sent_messages_codes
        mock_protobuf_to_dict.assert_not_called()

    def test_parse_packet_camera_notification_current_capture_status(
        self, mock_protobuf_to_dict, _mock_send_packet
    ):
        from insta360.pb2 import current_capture_status_pb2

        expected_message = current_capture_status_pb2.CaptureStatus()
        expected_message.state = 1
        expected_message.capture_time = 1713511786
        expected_message.keyTimePointDetail = "Test Detail"

        seq = 1001
        pkt_data = (
            b"\x04\x00\x00"  # response_type
            b"\x10\x20"  # response_code
            b"\x02"  # unknown_1
            b"\xe9\x03\x00"  # response_seq
            b"\xe9\x03\x00"  # unknowns
            b"\x08\x01\x10\xea\xba\x88\xb1\x06\x1a\x0bTest Detail"  # Body
        )
        self.camera.sent_messages_codes[seq] = self.camera.PHONE_COMMAND_SET_OPTIONS
        self.camera._parse_packet(pkt_data)
        assert seq in self.camera.sent_messages_codes

        mock_protobuf_to_dict.assert_called_with(expected_message, response_code=8208)

    def test_parse_packet_camera_notification_storage_update(
        self, mock_protobuf_to_dict, _mock_send_packet
    ):
        from insta360.pb2 import storage_update_pb2

        expected_message = storage_update_pb2.NotificationCardUpdate()
        expected_message.state = 1
        expected_message.location = 1

        seq = 1001
        pkt_data = (
            b"\x04\x00\x00"  # response_type
            b"\x06\x20"  # response_code
            b"\x02"  # unknown_1
            b"\xe9\x03\x00"  # response_seq
            b"\xe9\x03\x00"  # unknowns
            b"\x08\x01\x10\x01"  # Body
        )
        self.camera.sent_messages_codes[seq] = self.camera.PHONE_COMMAND_SET_OPTIONS
        self.camera._parse_packet(pkt_data)
        assert seq in self.camera.sent_messages_codes

        mock_protobuf_to_dict.assert_called_with(expected_message, response_code=8198)

    def test_parse_packet_seq_not_in_sent_messages_codes(
        self, mock_protobuf_to_dict, _mock_send_packet
    ):
        seq = 1001
        pkt_data = (
            b"\x04\x00\x00"  # response_type
            b"\xc8\x00"  # response_code
            b"\x02"  # unknown_1
            b"\xe9\x03\x00"  # response_seq
            b"\xe9\x03\x00"  # unknowns
            b"\x08\x01\x10\x01"  # Body
        )

        assert seq not in self.camera.sent_messages_codes
        self.camera._parse_packet(pkt_data)
        mock_protobuf_to_dict.assert_not_called()

    def test_parse_packet_phone_command_get_options(
        self, mock_protobuf_to_dict, _mock_send_packet
    ):
        from insta360.pb2 import get_options_pb2

        seq = 1001
        pkt_data = (
            b"\x04\x00\x00"  # response_type
            b"\xc8\x00"  # response_code
            b"\x02"  # unknown_1
            b"\xe9\x03\x00"  # response_seq
            b"\xe9\x03\x00"  # unknowns
            b"\n\x0b\x01\x02\x03\x04\x05\x0b\x0c\r\x0f\x10\x14\x12\x04\x08\x01\x10\x01"  # Body
        )
        self.camera.sent_messages_codes[seq] = self.camera.PHONE_COMMAND_GET_OPTIONS
        self.camera._parse_packet(pkt_data)
        assert seq not in self.camera.sent_messages_codes

        mock_protobuf_to_dict.assert_called_once()
        call_args = mock_protobuf_to_dict.call_args

        assert isinstance(call_args[0][0], get_options_pb2.GetOptionsResp)
        assert call_args[1] == {
            "message_code": self.camera.PHONE_COMMAND_GET_OPTIONS,
            "response_code": self.camera.RESPONSE_CODE_OK,
        }

    def test_parse_packet_phone_command_set_options(
        self, mock_protobuf_to_dict, _mock_send_packet
    ):
        from insta360.pb2 import set_options_pb2

        seq = 1001
        pkt_data = (
            b"\x04\x00\x00"  # response_type
            b"\xc8\x00"  # response_code
            b"\x02"  # unknown_1
            b"\xe9\x03\x00"  # response_seq
            b"\xe9\x03\x00"  # unknowns
            b"\n\x0b\x01\x02\x03\x04\x05\x0b\x0c\r\x0f\x10\x14"  # Body
        )
        self.camera.sent_messages_codes[seq] = self.camera.PHONE_COMMAND_SET_OPTIONS
        self.camera._parse_packet(pkt_data)
        assert seq not in self.camera.sent_messages_codes

        mock_protobuf_to_dict.assert_called_once()
        call_args = mock_protobuf_to_dict.call_args

        assert isinstance(call_args[0][0], set_options_pb2.SetOptionsResp)
        assert call_args[1] == {
            "message_code": self.camera.PHONE_COMMAND_SET_OPTIONS,
            "response_code": self.camera.RESPONSE_CODE_OK,
        }

    def test_parse_packet_phone_command_get_file_list(
        self, mock_protobuf_to_dict, _mock_send_packet
    ):
        from insta360.pb2 import get_file_list_pb2

        seq = 1001
        pkt_data = (
            b"\x04\x00\x00"  # response_type
            b"\xc8\x00"  # response_code
            b"\x02"  # unknown_1
            b"\xe9\x03\x00"  # response_seq
            b"\xe9\x03\x00"  # unknowns
            b"\n\x17/Users/downloads/1.jpeg\n\x17/Users/downloads/2.jpeg\x10\n"  # Body
        )
        self.camera.sent_messages_codes[seq] = self.camera.PHONE_COMMAND_GET_FILE_LIST
        self.camera._parse_packet(pkt_data)
        assert seq not in self.camera.sent_messages_codes

        mock_protobuf_to_dict.assert_called_once()
        call_args = mock_protobuf_to_dict.call_args

        assert isinstance(call_args[0][0], get_file_list_pb2.GetFileListResp)
        assert call_args[1] == {
            "message_code": self.camera.PHONE_COMMAND_GET_FILE_LIST,
            "response_code": self.camera.RESPONSE_CODE_OK,
        }

    def test_parse_packet_phone_command_stop_capture(
        self, mock_protobuf_to_dict, _mock_send_packet
    ):
        from insta360.pb2 import stop_capture_pb2

        seq = 1001
        pkt_data = (
            b"\x04\x00\x00"  # response_type
            b"\xc8\x00"  # response_code
            b"\x02"  # unknown_1
            b"\xe9\x03\x00"  # response_seq
            b"\xe9\x03\x00"  # unknowns
            b"\n\x1d\n\x16/Users/downloads/1.mp4\x10\xfa\x06\x18\n"  # Body
        )
        self.camera.sent_messages_codes[seq] = self.camera.PHONE_COMMAND_STOP_CAPTURE
        self.camera._parse_packet(pkt_data)
        assert seq not in self.camera.sent_messages_codes

        mock_protobuf_to_dict.assert_called_once()
        call_args = mock_protobuf_to_dict.call_args

        assert isinstance(call_args[0][0], stop_capture_pb2.StopCaptureResp)
        assert call_args[1] == {
            "message_code": self.camera.PHONE_COMMAND_STOP_CAPTURE,
            "response_code": self.camera.RESPONSE_CODE_OK,
        }

    def test_parse_packet_phone_command_take_picture(
        self, mock_protobuf_to_dict, _mock_send_packet
    ):
        from insta360.pb2 import take_picture_pb2

        seq = 1001
        pkt_data = (
            b"\x04\x00\x00"  # response_type
            b"\xc8\x00"  # response_code
            b"\x02"  # unknown_1
            b"\xe9\x03\x00"  # response_seq
            b"\xe9\x03\x00"  # unknowns
            # body
            b"\n3\n\x17/users/downloads/1.jpeg\x10\xf4\x03\x1a\x15\xfe\xeb\x1e\xae\xcf\xdd\xa3\t\xe5\xa1\xa7l\xff_"
            b"\xed\x86\xe9\x9b\x8e\x97\xa0\x123\n\x17/users/downloads/1.jpeg\x10\xf4\x03\x1a\x15\xfe\xeb\x1e\xae\xcf"
            b"\xdd\xa3\t\xe5\xa1\xa7l\xff_\xed\x86\xe9\x9b\x8e\x97\xa0\x1a3\n\x17/users/downloads/1.jpeg\x10\xf4\x03"
            b"\x1a\x15\xfe\xeb\x1e\xae\xcf\xdd\xa3\t\xe5\xa1\xa7l\xff_\xed\x86\xe9\x9b\x8e\x97\xa0"
        )
        self.camera.sent_messages_codes[seq] = self.camera.PHONE_COMMAND_TAKE_PICTURE
        self.camera._parse_packet(pkt_data)
        assert seq not in self.camera.sent_messages_codes

        mock_protobuf_to_dict.assert_called_once()
        call_args = mock_protobuf_to_dict.call_args

        assert isinstance(call_args[0][0], take_picture_pb2.TakePictureResponse)
        assert call_args[1] == {
            "message_code": self.camera.PHONE_COMMAND_TAKE_PICTURE,
            "response_code": self.camera.RESPONSE_CODE_OK,
        }

    def test_parse_packet_phone_command_get_photography_options(
        self, mock_protobuf_to_dict, _mock_send_packet
    ):
        from insta360.pb2 import get_photography_options_pb2

        seq = 1001
        pkt_data = (
            b"\x04\x00\x00"  # response_type
            b"\xc8\x00"  # response_code
            b"\x02"  # unknown_1
            b"\xe9\x03\x00"  # response_seq
            b"\xe9\x03\x00"  # unknowns
            b"\n\x02\x02\x03\x12\x03\x10\xc8\x01"  # Body
        )
        self.camera.sent_messages_codes[seq] = (
            self.camera.PHONE_COMMAND_GET_PHOTOGRAPHY_OPTIONS
        )
        self.camera._parse_packet(pkt_data)
        assert seq not in self.camera.sent_messages_codes

        mock_protobuf_to_dict.assert_called_once()
        call_args = mock_protobuf_to_dict.call_args

        assert isinstance(
            call_args[0][0], get_photography_options_pb2.GetPhotographyOptionsResp
        )
        assert call_args[1] == {
            "message_code": self.camera.PHONE_COMMAND_GET_PHOTOGRAPHY_OPTIONS,
            "response_code": self.camera.RESPONSE_CODE_OK,
        }

    def test_parse_packet_phone_command_get_current_capture_status(
        self, mock_protobuf_to_dict, _mock_send_packet
    ):
        from insta360.pb2 import get_current_capture_status_pb2

        seq = 1001
        pkt_data = (
            b"\x04\x00\x00"  # response_type
            b"\xc8\x00"  # response_code
            b"\x02"  # unknown_1
            b"\xe9\x03\x00"  # response_seq
            b"\xe9\x03\x00"  # unknowns
            b'\n\x0c\x08\x01\x10d\x18\x02"\x04Test'  # Body
        )
        self.camera.sent_messages_codes[seq] = (
            self.camera.PHONE_COMMAND_GET_CURRENT_CAPTURE_STATUS
        )
        self.camera._parse_packet(pkt_data)
        assert seq not in self.camera.sent_messages_codes

        mock_protobuf_to_dict.assert_called_once()
        call_args = mock_protobuf_to_dict.call_args

        assert isinstance(
            call_args[0][0], get_current_capture_status_pb2.GetCurrentCaptureStatusResp
        )
        assert call_args[1] == {
            "message_code": self.camera.PHONE_COMMAND_GET_CURRENT_CAPTURE_STATUS,
            "response_code": self.camera.RESPONSE_CODE_OK,
        }

    def test_parse_packet_live_stream_dump(
        self, _mock_protobuf_to_dict, _mock_send_packet
    ):
        seq = 1001
        pkt_data = (
            b"\x01\x00\x00"  # response_type
            b"\xc8\x00"  # response_code
            b"\x02"  # unknown_1
            b"\xe9\x03\x00"  # response_seq
            b"\xe9\x03\x00"  # unknowns
            b'\n\x0c\x08\x01\x10d\x18\x02"\x04Test'  # Body
        )
        self.camera.sent_messages_codes[seq] = (
            self.camera.PHONE_COMMAND_START_LIVE_STREAM
        )
        with patch.object(self.camera, "dump_fp", MagicMock()) as mock_open_dump:
            self.camera._parse_packet(pkt_data)
            mock_open_dump.write.assert_called_with(
                b'\n\x0c\x08\x01\x10d\x18\x02"\x04Test'
            )
