import logging
import unittest

from insta360.rtmp import Client
from unit_tests.common import ensure_camera_connected
from unittest.mock import patch


class TestCameraOpen(unittest.TestCase):
    @patch(
        "insta360.rtmp.rtmp.ensure_camera_connected",
        side_effect=ensure_camera_connected,
    )
    def test_camera_init_custom_logger(self, _mock_ensure_camera_connected):
        logger = logging.getLogger("insta360")
        camera = Client(logger=logger)
        assert camera.logger is logger
