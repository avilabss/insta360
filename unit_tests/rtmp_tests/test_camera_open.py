import signal
import socket
import time
import logging
import os

import unittest
from unittest.mock import ANY, MagicMock, Mock, patch
from unit_tests.common import ensure_camera_connected
from insta360.rtmp import Client, CommandFailedException, CameraNotConnectedException


@patch("socket.socket", return_value=Mock())
@patch("select.poll", return_value=Mock())
@patch("insta360.rtmp.Client.KeepAliveTimer")
@patch("insta360.rtmp.Client._send_message")
class TestCameraOpen(unittest.TestCase):
    @patch(
        "insta360.rtmp.rtmp.ensure_camera_connected",
        side_effect=ensure_camera_connected,
    )
    def setUp(self, _mock_ensure_camera_connected) -> None:
        logging.getLogger().setLevel(logging.ERROR)
        self.camera = Client()
        return super().setUp()

    def tearDown(self) -> None:
        self.camera = None
        return super().tearDown()

    @patch("insta360.rtmp.Client._send_packet")
    def test_open(
        self,
        mock_send_packet,
        mock_send_message,
        _mock_keepalivetimer,
        mock_poll,
        mock_socket_socket,
    ):
        mock_poll.return_value.poll.return_value = []

        self.camera.open()

        mock_socket_socket.assert_called_with(socket.AF_INET, socket.SOCK_STREAM)
        self.camera.camera_socket.connect.assert_called_with(
            (self.camera.connect_host, self.camera.connect_port)
        )
        mock_send_packet.assert_any_call(self.camera.PKT_SYNC)
        mock_send_packet.assert_any_call(self.camera.PKT_KEEPALIVE)
        mock_send_message.assert_called_with(ANY, self.camera.PHONE_COMMAND_SET_OPTIONS)
        self.camera.timer_keepalive.start.assert_called()
        self.camera.close()

    @patch("insta360.rtmp.Client.close")
    def test_signal_handler_SIGTERM(
        self,
        mock_close,
        _mock_send_message,
        _mock_keepalivetimer,
        mock_poll,
        _mock_socket_socket,
    ):
        mock_poll.return_value.poll.return_value = []

        self.camera.open()
        pid = os.getpid()
        with self.assertRaises(SystemExit):
            os.kill(pid, signal.SIGTERM)

        mock_close.assert_called()

    @patch("insta360.rtmp.Client.close")
    def test_signal_handler_SIGINT(
        self,
        mock_close,
        _mock_send_message,
        _mock_keepalivetimer,
        mock_poll,
        _mock_socket_socket,
    ):
        mock_poll.return_value.poll.return_value = []

        self.camera.open()
        pid = os.getpid()
        with self.assertRaises(SystemExit):
            os.kill(pid, signal.SIGINT)

        mock_close.assert_called()

    def test_open_socket_connect_exception(
        self,
        _mock_send_message,
        _mock_keepalivetimer,
        mock_poll,
        mock_socket_socket,
    ):
        mock_poll.return_value.poll.return_value = []
        mock_socket_socket.return_value.connect.side_effect = socket.error()
        self.camera.logger.error = MagicMock()

        self.camera.open()
        assert self.camera.camera_socket is None

        self.camera.close()

        self.camera.logger.error.assert_any_call("Exception in socket.connect(): ")

    @patch("insta360.rtmp.Client._check_if_command_successful")
    def test_open_connect_to_camera_failed(
        self,
        mock_check_if_command_successful,
        _mock_send_message,
        _mock_keepalivetimer,
        mock_poll,
        _mock_socket_socket,
    ):
        mock_poll.return_value.poll.return_value = []
        mock_check_if_command_successful.side_effect = CommandFailedException()

        with self.assertRaises(CommandFailedException):
            self.camera.open()

        self.camera.close()

    def test_check_if_command_successful_failure(
        self,
        _mock_send_message,
        _mock_keepalivetimer,
        mock_poll,
        _mock_socket_socket,
    ):
        mock_poll.return_value.poll.return_value = []
        self.camera.sent_messages_codes = [1]
        with self.assertRaises(CommandFailedException):
            self.camera._check_if_command_successful(1, wait_for_seconds=1)

    def test_send_packet(
        self,
        _mock_send_message,
        _mock_keepalivetimer,
        mock_poll,
        mock_socket_socket,
    ):
        mock_poll.return_value.poll.return_value = []
        mock_socket_socket.return_value = Mock()
        self.camera.open()
        with patch.object(
            self.camera, "_socket_send", return_value=Mock()
        ) as mock_socket_send:
            self.camera._send_packet(self.camera.PKT_SYNC)
            mock_socket_send.assert_called_once()
        self.camera.close()

    def test_socket_send(
        self,
        _mock_send_message,
        _mock_keepalivetimer,
        mock_poll,
        mock_socket_socket,
    ):
        mock_poll.return_value.poll.return_value = []
        mock_socket_socket.return_value = Mock()
        self.camera.open()

        with patch(
            "insta360.rtmp.threading.Lock", return_value=MagicMock()
        ) as mock_lock:
            self.camera.socket_lock = mock_lock
            result = self.camera._socket_send(b"abcde")
            assert result is True
            self.camera.camera_socket.sendall.assert_called_with(b"abcde")

        self.camera.close()

    def test_socket_send_failed(
        self,
        _mock_send_message,
        _mock_keepalivetimer,
        mock_poll,
        mock_socket_socket,
    ):
        mock_poll.return_value.poll.return_value = []
        mock_socket_socket.return_value = Mock()
        self.camera.open()
        self.camera.camera_socket.sendall.side_effect = socket.error()

        with patch(
            "insta360.rtmp.threading.Lock", return_value=MagicMock()
        ) as mock_lock:
            self.camera.socket_lock = mock_lock
            result = self.camera._socket_send(b"abcde")
            assert result is False
            self.camera.camera_socket.sendall.assert_called_with(b"abcde")

        self.camera.close()

    def test_keep_alive_is_connected_timedout(
        self,
        _mock_send_message,
        _mock_keepalivetimer,
        mock_poll,
        mock_socket_socket,
    ):
        mock_poll.return_value.poll.return_value = []
        mock_socket_socket.return_value = Mock()
        self.camera.open()
        self.camera.is_connected = True
        self.camera.last_pkt_recv_time = time.time() - (
            self.camera.IS_CONNECTED_TIMEOUT_SEC + 10
        )
        self.camera._keep_alive()
        assert self.camera.is_connected is False

    @patch("insta360.rtmp.Client._send_packet")
    def test_keep_alive_is_connected_not_timedout(
        self,
        mock_send_packet,
        _mock_send_message,
        _mock_keepalivetimer,
        mock_poll,
        mock_socket_socket,
    ):
        mock_poll.return_value.poll.return_value = []
        mock_socket_socket.return_value = Mock()
        self.camera.open()
        self.camera.is_connected = True
        self.camera.last_pkt_sent_time = time.time() - (
            self.camera.KEEPALIVE_INTERVAL_SEC + 2
        )
        self.camera._keep_alive()
        assert self.camera.is_connected is True
        mock_send_packet.assert_called_with(self.camera.PKT_KEEPALIVE)

    @patch("insta360.rtmp.Client.open")
    def test_keep_alive_is_not_connected(
        self,
        mock_open,
        _mock_send_message,
        _mock_keepalivetimer,
        mock_poll,
        mock_socket_socket,
    ):
        mock_poll.return_value.poll.return_value = []
        mock_socket_socket.return_value = Mock()
        self.camera.is_connected = False
        self.camera.reconnect_time = time.time() - (
            self.camera.RECONNECT_TIMEOUT_SEC + 2
        )
        self.camera._keep_alive()
        mock_open.assert_called_once()
