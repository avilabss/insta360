import unittest

from insta360.rtmp import Client
from insta360.utils.utilities import protobuf_to_dict
from unittest.mock import patch
from unit_tests.common import ensure_camera_connected


class TestUtils(unittest.TestCase):
    @patch(
        "insta360.rtmp.rtmp.ensure_camera_connected",
        side_effect=ensure_camera_connected,
    )
    def test_protobuf_to_dict(self, _mock_ensure_camera_connected):
        from insta360.pb2 import get_options_pb2

        body = (
            b"\n\x0b\x01\x02\x03\x04\x05\x0b\x0c\r\x0f\x10\x14\x12\x04\x08\x01\x10\x01"
        )
        message = Client()._parse_protobuf_message(
            get_options_pb2.GetOptionsResp(), body
        )

        res = protobuf_to_dict(message, response_code=200, message_code=8)
        assert isinstance(res, dict)
        assert res["optionTypes"] == [
            "VIDEO_RESOLUTION",
            "PHOTO_SIZE",
            "VIDEO_BITRATE",
            "AUDIO_BITRATE",
            "AUDIO_SAMPLERATE",
            "BATTERY_STATUS",
            "LOCAL_TIME",
            "TIME_ZONE",
            "SERIAL_NUMBER",
            "UUID",
            "STORAGE_STATE",
        ]
        assert res["response_code"] == 200
        assert res["message_code"] == 8
