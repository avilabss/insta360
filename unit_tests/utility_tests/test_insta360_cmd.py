import unittest
import argparse

from unittest.mock import patch, MagicMock
from insta360_cmd import handle_camera_commands


class TestInsta360CMD(unittest.TestCase):
    @patch("insta360_cmd.Client")
    @patch("insta360_cmd.logger")
    def test_handle_camera_commands_livestream(self, _mock_logger, _mock_client):
        mock_client_instance = MagicMock()
        _mock_client.return_value = mock_client_instance

        args = argparse.Namespace(
            command=["livestream"],
            output="./output/dump.mp4",
            display=False,
            no_hwaccel=False,
        )

        handle_camera_commands(args)

        _mock_client.assert_called_once_with(
            host="192.168.42.1",
            port=6666,
            output_filepath=args.output,
            enable_hwaccel=True,
        )
        mock_client_instance.open.assert_called_once()
        mock_client_instance.start_live_stream.assert_called_once()
        mock_client_instance.start_preview_stream.assert_not_called()
        mock_client_instance.start_capture.assert_not_called()

        _mock_logger.info.assert_any_call("Connecting to camera")
        _mock_logger.info.assert_any_call("Starting livestream")
        _mock_logger.info.assert_any_call("Commands executed successfully")
        _mock_logger.info.assert_any_call(
            "Press Ctrl+C to stop the commands and close the connection"
        )

    @patch("insta360_cmd.Client")
    @patch("insta360_cmd.logger")
    def test_handle_camera_commands_preview(self, _mock_logger, _mock_client):
        mock_client_instance = MagicMock()
        _mock_client.return_value = mock_client_instance

        args = argparse.Namespace(
            command=["preview"],
            output="./output/dump.mp4",
            display=False,
            no_hwaccel=False,
        )

        handle_camera_commands(args)

        _mock_client.assert_called_once_with(
            host="192.168.42.1",
            port=6666,
            output_filepath=args.output,
            enable_hwaccel=True,
        )
        mock_client_instance.open.assert_called_once()
        mock_client_instance.start_live_stream.assert_not_called()
        mock_client_instance.start_preview_stream.assert_called_once()
        mock_client_instance.start_capture.assert_not_called()

        _mock_logger.info.assert_any_call("Connecting to camera")
        _mock_logger.info.assert_any_call("Starting preview stream")
        _mock_logger.info.assert_any_call("Commands executed successfully")
        _mock_logger.info.assert_any_call(
            "Press Ctrl+C to stop the commands and close the connection"
        )

    @patch("insta360_cmd.Client")
    @patch("insta360_cmd.logger")
    def test_handle_camera_commands_capture(self, _mock_logger, _mock_client):
        mock_client_instance = MagicMock()
        _mock_client.return_value = mock_client_instance

        args = argparse.Namespace(
            command=["capture"],
            output="./output/dump.mp4",
            display=False,
            no_hwaccel=False,
        )

        handle_camera_commands(args)

        _mock_client.assert_called_once_with(
            host="192.168.42.1",
            port=6666,
            output_filepath=args.output,
            enable_hwaccel=True,
        )
        mock_client_instance.open.assert_called_once()
        mock_client_instance.start_live_stream.assert_not_called()
        mock_client_instance.start_preview_stream.assert_not_called()
        mock_client_instance.start_capture.assert_called_once()

        _mock_logger.info.assert_any_call("Connecting to camera")
        _mock_logger.info.assert_any_call("Capturing video")
        _mock_logger.info.assert_any_call("Commands executed successfully")
        _mock_logger.info.assert_any_call(
            "Press Ctrl+C to stop the commands and close the connection"
        )

    @patch("insta360_cmd.Client")
    @patch("insta360_cmd.logger")
    def test_handle_camera_commands_capture_and_livestream(
        self, _mock_logger, _mock_client
    ):
        mock_client_instance = MagicMock()
        _mock_client.return_value = mock_client_instance

        args = argparse.Namespace(
            command=["capture", "livestream"],
            output="./output/dump.mp4",
            display=False,
            no_hwaccel=False,
        )

        handle_camera_commands(args)

        _mock_client.assert_called_once_with(
            host="192.168.42.1",
            port=6666,
            output_filepath=args.output,
            enable_hwaccel=True,
        )
        mock_client_instance.open.assert_called_once()
        mock_client_instance.start_live_stream.assert_called_once()
        mock_client_instance.start_preview_stream.assert_not_called()
        mock_client_instance.start_capture.assert_called_once()

        _mock_logger.info.assert_any_call("Connecting to camera")
        _mock_logger.info.assert_any_call("Starting livestream")
        _mock_logger.info.assert_any_call("Capturing video")
        _mock_logger.info.assert_any_call("Commands executed successfully")
        _mock_logger.info.assert_any_call(
            "Press Ctrl+C to stop the commands and close the connection"
        )

    @patch("insta360_cmd.Client")
    @patch("insta360_cmd.logger")
    def test_handle_camera_commands_capture_and_preview(
        self, _mock_logger, _mock_client
    ):
        mock_client_instance = MagicMock()
        _mock_client.return_value = mock_client_instance

        args = argparse.Namespace(
            command=["capture", "preview"],
            output="./output/dump.mp4",
            display=False,
            no_hwaccel=False,
        )

        handle_camera_commands(args)

        _mock_client.assert_called_once_with(
            host="192.168.42.1",
            port=6666,
            output_filepath=args.output,
            enable_hwaccel=True,
        )
        mock_client_instance.open.assert_called_once()
        mock_client_instance.start_live_stream.assert_not_called()
        mock_client_instance.start_preview_stream.assert_called_once()
        mock_client_instance.start_capture.assert_called_once()

        _mock_logger.info.assert_any_call("Connecting to camera")
        _mock_logger.info.assert_any_call("Starting preview stream")
        _mock_logger.info.assert_any_call("Capturing video")
        _mock_logger.info.assert_any_call("Commands executed successfully")
        _mock_logger.info.assert_any_call(
            "Press Ctrl+C to stop the commands and close the connection"
        )
