import unittest

from tools.packet_analyzer import PhoneCommandPacket, ReceivedPacket, PacketUtils

from insta360.pb2 import start_live_stream_pb2


class TestPacketAnalyzer(unittest.TestCase):
    def setUp(self) -> None:
        return super().setUp()

    def tearDown(self) -> None:
        return super().tearDown()

    def test_get_packet_type(self):
        response_type = b"\x04\x00\x00"
        result = PacketUtils().get_packet_type(response_type)
        self.assertEqual(result, "PHONE_COMMAND")

    def test_get_message_code_type(self):
        message_code = 1
        result = PacketUtils().get_message_code_type(message_code)
        self.assertEqual(result, "PHONE_COMMAND_START_LIVE_STREAM")

    def test_get_response_code_type(self):
        response_code = 200
        result = PacketUtils().get_response_code_type(response_code)
        self.assertEqual(result, "OK")

    def test_get_proto_class(self):
        message_code = 1
        expected = start_live_stream_pb2.StartLiveStream()
        result = PacketUtils().get_proto_class(message_code)
        self.assertEqual(result, expected)

    def test_received_packet(self):
        raw_packet = b"\x31\x00\x00\x00\x04\x00\x00\xc8\x00\x02\x15\x00\x00\x80\x00\\\x08\x0b\x08\x14\x12\x1bZ\x04\x08\x00\x10C\xa2\x01\x12\x08\x00\x10\x80\x80\xd8\xdf\xe4\x01\x18\x80\x80\xcf\xeb\x01"
        result = ReceivedPacket(raw_packet)
        self.assertEqual(result.packet_length, 49)
        self.assertEqual(result.response_code, 200)
        self.assertEqual(result.response_code_str, "OK")
        self.assertEqual(result.response_type, "PHONE_COMMAND")

    def test_phone_command_packet(self):
        raw_packet = b"\x18\x00\x00\x00\x04\x00\x00\x0d\x00\x02\x17\x00\x00\x80\x00\x00\x08\x01\x18\xff\xff\xff\xff\x07"
        result = PhoneCommandPacket(raw_packet)
        self.assertEqual(result.message_code, 13)
        self.assertEqual(result.message_type, "PHONE_COMMAND")
        self.assertEqual(result.message_code_str, "PHONE_COMMAND_GET_FILE_LIST")
        self.assertEqual(result.body, {"limit": 2147483647, "mediaType": "PHOTO"})
