"""
This tool is intended to help analyze packets sent by the app to the camera and vice versa.
The end goal is to be able to pass a wireshark capture to this tool and have an output that is parsed and human readable.
The current implementation is a work in progress and only supports a few packet types.
It also does not support analysis of entire wireshark captures.
But as a proof of concept, it can be used to analyze payloads of individual packets.

# Using this tool is simple.
1. Capture packets using wireshark.
2. Select a packet of interest.
3. Right click on Data field of the packet and select "Copy" -> "Value"
4. Use the copied value as the 'payload' (--payload or -p) for this tool.
5. Identify if the packet is 'sent' from the app to the camera or 'received' from the camera to the app.
6. Use the '-t' or '--type' flag to specify the type of packet.

Example:
    ```bash
    python3 ./tools/packet_analyzer.py -t s -p 1800000400000d0002170000800000080118ffffffff07
    ```

Although this is intended to be used as a tool for developing the main `insta360.rtmp` module, there's still a lot of improvements that can be made to this tool and we would appreciate any contributions.
For the same reason, we have also laid out all the intricacies of this tool below.

Classes:
    PacketUtils: Utility class for all packets.
    ReceivedPacket: Model for received packets.
    SyncPacket: Model for sync packets.
    KeepAlivePacket: Model for keep alive packets.
    PhoneCommandPacket: Model for phone command packets.

Attributes:
    ALL_PROTO_CLASSES: List of all protobuf classes.
    PACKET_ID_MAP: Mapping of packet types to their respective values.
    MESSAGE_CODE_ID_MAP: Mapping of message codes to their respective values.
    MESSAGE_CODE_TO_PROTO_CLASS_MAP: Mapping of message codes to their respective protobuf classes.
    RESPONSE_CODE_ID_MAP: Mapping of response codes to their respective values.
"""

import struct
import logging
import pprint
import os
import importlib
import inspect

from google.protobuf import json_format
from argparse import ArgumentParser
from insta360.utils.utilities import bytes_to_hexascii, bytes_to_hex

from insta360.pb2 import start_live_stream_pb2
from insta360.pb2 import stop_live_stream_pb2
from insta360.pb2 import take_picture_pb2
from insta360.pb2 import start_capture_pb2
from insta360.pb2 import stop_capture_pb2
from insta360.pb2 import set_options_pb2
from insta360.pb2 import set_photography_options_pb2
from insta360.pb2 import get_options_pb2
from insta360.pb2 import get_photography_options_pb2
from insta360.pb2 import get_file_list_pb2
from insta360.pb2 import get_current_capture_status_pb2


# Set up logging
logging.basicConfig(format="%(levelname)-4s: %(message)s", level=logging.DEBUG)
logger = logging.getLogger("packet_analizer")


# Load all proto modules
CURRENT_DIR = os.getcwd()
PROTO_MODULES_DIR = os.path.join(CURRENT_DIR, "insta360", "pb2")
PROTO_FILES = [
    f for f in os.listdir(PROTO_MODULES_DIR) if f.endswith(".py") and f != "__init__.py"
]
PROTO_MODULES = [importlib.import_module(f"insta360.pb2.{f[:-3]}") for f in PROTO_FILES]
ALL_PROTO_CLASSES = []


def get_classes(module):
    return [m[1] for m in inspect.getmembers(module, inspect.isclass)]


for module in PROTO_MODULES:
    classes = get_classes(module)
    if not classes:
        continue
    for aclass in classes:
        if aclass in ALL_PROTO_CLASSES:
            continue
        ALL_PROTO_CLASSES.append(aclass)


# Identifcation maps
PACKET_ID_MAP = {
    b"\x06\x00\x00": "SYNC",
    b"\x05\x00\x00": "KEEP_ALIVE",
    b"\x01\x00\x00": "RAW_STREAM",
    b"\x04\x00\x00": "PHONE_COMMAND",
}

MESSAGE_CODE_ID_MAP = {
    0: "PHONE_COMMAND_BEGIN",
    1: "PHONE_COMMAND_START_LIVE_STREAM",
    2: "PHONE_COMMAND_STOP_LIVE_STREAM",
    3: "PHONE_COMMAND_TAKE_PICTURE",
    4: "PHONE_COMMAND_START_CAPTURE",
    5: "PHONE_COMMAND_STOP_CAPTURE",
    6: "PHONE_COMMAND_CANCEL_CAPTURE",
    7: "PHONE_COMMAND_SET_OPTIONS",
    8: "PHONE_COMMAND_GET_OPTIONS",
    9: "PHONE_COMMAND_SET_PHOTOGRAPHY_OPTIONS",
    10: "PHONE_COMMAND_GET_PHOTOGRAPHY_OPTIONS",
    11: "PHONE_COMMAND_GET_FILE_EXTRA",
    12: "PHONE_COMMAND_DELETE_FILES",
    13: "PHONE_COMMAND_GET_FILE_LIST",
    15: "PHONE_COMMAND_GET_CURRENT_CAPTURE_STATUS",
}

MESSAGE_CODE_TO_PROTO_CLASS_MAP = {
    1: start_live_stream_pb2.StartLiveStream(),
    2: stop_live_stream_pb2.StopLiveStream(),
    3: take_picture_pb2.TakePicture(),
    4: start_capture_pb2.StartCapture(),
    5: stop_capture_pb2.StopCapture(),
    7: set_options_pb2.SetOptions(),
    8: get_options_pb2.GetOptions(),
    9: set_photography_options_pb2.SetPhotographyOptions(),
    10: get_photography_options_pb2.GetPhotographyOptions(),
    13: get_file_list_pb2.GetFileList(),
    15: get_current_capture_status_pb2.CameraCaptureStatus(),
}

RESPONSE_CODE_ID_MAP = {
    200: "OK",
    500: "RESPONSE_CODE_ERROR",
    8196: "CAMERA_NOTIFICATION_BATTERY_LOW",
    8198: "CAMERA_NOTIFICATION_STORAGE_UPDATE",
    8199: "CAMERA_NOTIFICATION_STORAGE_FULL",
    8201: "CAMERA_NOTIFICATION_CAPTURE_STOPPED",
    8208: "CAMERA_NOTIFICATION_CURRENT_CAPTURE_STATUS",
}


# Models
class PacketUtils:
    """
    Base utility class for all packets.

    Methods:
        get_packet_type: Get the type of packet.
        get_message_code_type: Get the type of message code.
        get_response_code_type: Get the type of response code.
        get_proto_class: Get the protobuf class for a message code.
        parse_against_all_proto_classes: Parse the raw body against all protobuf classes.
    """

    def get_packet_type(self, packet: bytes) -> str:
        """
        Get the type of packet from `PACKET_ID_MAP`.

        Parameters:
            packet: Raw bytes of the packet.

        Returns:
            Packet type if matched, else 'UNKNOWN'.
        """

        try:
            return PACKET_ID_MAP[packet]
        except KeyError:
            return "UNKNOWN"

    def get_message_code_type(self, message_code: int) -> str:
        """
        Get the type of message code from `MESSAGE_CODE_ID_MAP`.

        Parameters:
            message_code: Message code.

        Returns:
            Message code type if matched, else 'UNKNOWN'.
        """

        try:
            return MESSAGE_CODE_ID_MAP[message_code]
        except KeyError:
            return "UNKNOWN"

    def get_response_code_type(self, response_code: int) -> str:
        """
        Get the type of response code from `RESPONSE_CODE_ID_MAP`.

        Parameters:
            response_code: Response code.

        Returns:
            Response code type if matched, else 'UNKNOWN'.
        """

        try:
            return RESPONSE_CODE_ID_MAP[response_code]
        except KeyError:
            return "UNKNOWN"

    def get_proto_class(self, message_code: int):
        """
        Get the protobuf class for a message code.

        Parameters:
            message_code: Message code.

        Returns:
            Protobuf class if matched, else None.
        """

        try:
            return MESSAGE_CODE_TO_PROTO_CLASS_MAP[message_code]
        except KeyError:
            return None

    def parse_against_all_proto_classes(self, raw_body: bytes):
        """
        Tries to parse the raw body against all protobuf classes.

        Parameters:
            raw_body: Raw body of the packet.

        Returns:
            List of possible protobuf classes.
        """

        possible_classes = []

        for proto_class in ALL_PROTO_CLASSES:
            try:
                init_class = proto_class()
                init_class.ParseFromString(raw_body)
                possible_classes.append(init_class)
            except Exception as e:
                pass

        return possible_classes


class ReceivedPacket(PacketUtils):
    """
    Model for received packets.

    Parameters:
        raw_packet: Raw bytes of the packet.

    Methods:
        pformat: Pretty format the packet.

    Example:
        ```python
        # python3 ./tools/packet_analyzer.py -t r -p 31000000040000c8000215000080005c080b0814121b5a0408001043a201120800108080d8dfe401188080cfec01

        {
            'ascii_body': '\\x08\\x0b\\x08\\x14\\x12\\x1bZ\\x04\\x08\\x00\\x10C\\xa2\\x01\\x12\\x08\\x00\\x10\\x80\\x80\\xd8\\xdf\\xe4\\x01\\x18\\x80\\x80\\xcf\\xec\\x01',
            'packet_length': 49,
            'raw_body': '\\x08\\x0b\\x08\\x14\\x12\\x1b\\x5a\\x04\\x08\\x00\\x10\\x43\\xa2\\x01\\x12\\x08\\x00\\x10\\x80\\x80\\xd8\\xdf\\xe4\\x01\\x18\\x80\\x80\\xcf\\xec\\x01',
            'raw_header': '\\x31\\x00\\x00\\x00\\x04\\x00\\x00\\xc8\\x00\\x02\\x15\\x00\\x00\\x80\\x00\\x5c',
            'raw_packet': '\\x31\\x00\\x00\\x00\\x04\\x00\\x00\\xc8\\x00\\x02\\x15\\x00\\x00\\x80\\x00\\x5c\\x08\\x0b\\x08\\x14\\x12\\x1b\\x5a\\x04\\x08\\x00\\x10\\x43\\xa2\\x01\\x12\\x08\\x00\\x10\\x80\\x80\\xd8\\xdf\\xe4\\x01\\x18\\x80\\x80\\xcf\\xec\\x01',
            'raw_packet_length': '\\x31\\x00\\x00\\x00',
            'raw_response_code': '\\xc8\\x00',
            'raw_response_seq': '\\x15\\x00\\x00\\x00',
            'raw_response_type': '\\x04\\x00\\x00',
            'response_code': 200,
            'response_code_str': 'OK',
            'response_seq': 21,
            'response_type': 'PHONE_COMMAND'
        }
        ```
    """

    def __init__(self, raw_packet: bytes):
        self.raw_packet = raw_packet
        self.raw_header = self.raw_packet[:16]
        self.raw_body = self.raw_packet[16:]

        self.raw_packet_length = self.raw_header[0:4]
        self.raw_response_type = self.raw_header[4:7]
        self.raw_response_code = self.raw_header[7:9]
        self.raw_response_seq = self.raw_header[10:13] + b"\x00"

        # Unkown fields
        _ = self.raw_header[9:10]
        _ = self.raw_header[13:14]
        _ = self.raw_header[14:15]
        _ = self.raw_header[15:16]

        # Unpack fields
        self.packet_length = struct.unpack("<I", self.raw_packet_length)[0]
        self.response_code = struct.unpack("<H", self.raw_response_code)[0]
        self.response_seq = struct.unpack("<I", self.raw_response_seq)[0]

        # Get possible packet type
        self.response_type = self.get_packet_type(self.raw_response_type)
        self.response_code_str = self.get_response_code_type(self.response_code)

    def pformat(self) -> str:
        """
        Pretty format the packet.

        Returns:
            Pretty formatted packet.
        """

        pretty_dict = {
            "raw_packet": bytes_to_hex(self.raw_packet),
            "raw_header": bytes_to_hex(self.raw_header),
            "raw_body": bytes_to_hex(self.raw_body),
            "ascii_body": bytes_to_hexascii(self.raw_body),
            "raw_packet_length": bytes_to_hex(self.raw_packet_length),
            "raw_response_type": bytes_to_hex(self.raw_response_type),
            "raw_response_code": bytes_to_hex(self.raw_response_code),
            "raw_response_seq": bytes_to_hex(self.raw_response_seq),
            "packet_length": self.packet_length,
            "response_code": self.response_code,
            "response_seq": self.response_seq,
            "response_type": self.response_type,
            "response_code_str": self.response_code_str,
        }

        return pprint.pformat(pretty_dict, compact=True)


class SyncPacket(PacketUtils):
    """
    Model for sync packets.

    Parameters:
        raw_packet: Raw bytes of the packet.

    Methods:
        pformat: Pretty format the packet.

    Example:
        ```python
        # python3 ./tools/packet_analyzer.py -t s -p 180000000400000d0002180000800000080318ffffffff07

        {
            'body': {'limit': 2147483647, 'mediaType': 'DNG'},
            'message_code': 13,
            'message_code_str': 'PHONE_COMMAND_GET_FILE_LIST',
            'message_seq': 24,
            'message_type': 'PHONE_COMMAND',
            'packet_length': 24,
            'proto_class': 'GetFileList',
            'raw_body': '\\x08\\x03\\x18\\xff\\xff\\xff\\xff\\x07',
            'raw_header': '\\x18\\x00\\x00\\x00\\x04\\x00\\x00\\x0d\\x00\\x02\\x18\\x00\\x00\\x80\\x00\\x00',
            'raw_message_code': '\\x0d\\x00',
            'raw_message_seq': '\\x18\\x00\\x00\\x00',
            'raw_message_type': '\\x04\\x00\\x00',
            'raw_packet': '\\x18\\x00\\x00\\x00\\x04\\x00\\x00\\x0d\\x00\\x02\\x18\\x00\\x00\\x80\\x00\\x00\\x08\\x03\\x18\\xff\\xff\\xff\\xff\\x07',
            'raw_packet_length': '\\x18\\x00\\x00\\x00'
        }
        ```
    """

    def __init__(self, raw_packet: bytes) -> None:
        self.raw_packet = raw_packet

        self.raw_packet_length = self.raw_packet[0:4]
        self.raw_message_type = self.raw_packet[4:7]
        self.raw_sync_code = self.raw_packet[7:17]

        self.packet_length = struct.unpack("<I", self.raw_packet_length)[0]
        self.message_type = self.get_packet_type(self.raw_message_type)

    def pformat(self) -> str:
        """
        Pretty format the packet.

        Returns:
            Pretty formatted packet.
        """

        pretty_dict = {
            "raw_packet": bytes_to_hex(self.raw_packet),
            "raw_packet_length": bytes_to_hex(self.raw_packet_length),
            "raw_message_type": bytes_to_hex(self.raw_message_type),
            "raw_sync_code": bytes_to_hex(self.raw_sync_code),
            "packet_length": self.packet_length,
            "message_type": self.message_type,
        }

        return pprint.pformat(pretty_dict, compact=True)


class KeepAlivePacket(PacketUtils):
    """
    Model for keep alive packets.

    Parameters:
        raw_packet: Raw bytes of the packet.

    Methods:
        pformat: Pretty format the packet.
    """

    def __init__(self, raw_packet: bytes) -> None:
        self.raw_packet = raw_packet
        self.raw_packet_length = self.raw_packet[0:4]
        self.raw_message_type = self.raw_packet[4:7]

        self.packet_length = struct.unpack("<I", self.raw_packet_length)[0]
        self.message_type = self.get_packet_type(self.raw_message_type)

    def pformat(self) -> str:
        """
        Pretty format the packet.

        Returns:
            Pretty formatted packet.
        """

        pretty_dict = {
            "raw_packet": bytes_to_hex(self.raw_packet),
            "raw_packet_length": bytes_to_hex(self.raw_packet_length),
            "raw_message_type": bytes_to_hex(self.raw_message_type),
            "packet_length": self.packet_length,
            "message_type": self.message_type,
        }

        return pprint.pformat(pretty_dict, compact=True)


class PhoneCommandPacket(PacketUtils):
    """
    Model for phone command packets.

    Parameters:
        raw_packet: Raw bytes of the packet.

    Methods:
        pformat: Pretty format the packet.
    """

    def __init__(self, raw_packet: bytes):
        self.raw_packet = raw_packet
        self.raw_header = self.raw_packet[:16]
        self.raw_body = self.raw_packet[16:]

        self.raw_packet_length = self.raw_header[0:4]
        self.raw_message_type = self.raw_header[4:7]
        self.raw_message_code = self.raw_header[7:9]
        self.raw_message_seq = self.raw_header[10:13] + b"\x00"

        # Unkown fields
        _ = self.raw_header[9:10]
        _ = self.raw_header[13:16]

        # Unpack fields
        self.packet_length = struct.unpack("<I", self.raw_packet_length)[0]
        self.message_code = struct.unpack("<H", self.raw_message_code)[0]
        self.message_seq = struct.unpack("<I", self.raw_message_seq)[0]

        # Get possible packet type
        self.message_type = self.get_packet_type(self.raw_message_type)
        self.message_code_str = self.get_message_code_type(self.message_code)

        # Unpack protobuf body
        self.proto_class = self.get_proto_class(self.message_code)
        self.body = None

        if self.proto_class is not None:
            try:
                self.proto_class.ParseFromString(self.raw_body)
                self.body = json_format.MessageToDict(self.proto_class)
            except Exception as e:
                logger.warning(
                    f"Failed to unpack protobuf body for {self.proto_class.__class__.__name__}: {e}"
                )

        if self.proto_class is None:
            try:
                possible_classes = self.parse_against_all_proto_classes(self.raw_body)

                for possible_class in possible_classes:
                    possible_payload = json_format.MessageToDict(possible_class)

                    logger.warning(
                        f"Found possible protobuf class: {possible_class.__class__.__name__}"
                    )
                    logger.warning(f"Possible payload: {possible_payload}")
                    logger.warning("-" * 50)

            except Exception as e:
                logger.warning(
                    f"Failed to find a suitable protobuf class for the body: {e}"
                )

    def pformat(self) -> str:
        """
        Pretty format the packet.

        Returns:
            Pretty formatted packet.
        """

        pretty_dict = {
            "raw_packet": bytes_to_hex(self.raw_packet),
            "raw_header": bytes_to_hex(self.raw_header),
            "raw_body": bytes_to_hex(self.raw_body),
            "raw_packet_length": bytes_to_hex(self.raw_packet_length),
            "raw_message_type": bytes_to_hex(self.raw_message_type),
            "raw_message_code": bytes_to_hex(self.raw_message_code),
            "raw_message_seq": bytes_to_hex(self.raw_message_seq),
            "packet_length": self.packet_length,
            "message_code": self.message_code,
            "message_seq": self.message_seq,
            "message_type": self.message_type,
            "message_code_str": self.message_code_str,
            "proto_class": self.proto_class.__class__.__name__,
            "body": self.body,
        }

        return pprint.pformat(pretty_dict, compact=True)


# Functions
def parse_args():
    """
    Parse command line arguments.

    Returns:
        Parsed arguments.
    """

    parser = ArgumentParser(description="Packet Analyzer")
    parser.add_argument(
        "-p", "--payload", type=str, required=True, help="Packet payload to analyze"
    )
    parser.add_argument(
        "-t",
        "--type",
        type=str,
        required=True,
        help="Packet type",
        choices=["sent", "received", "s", "r"],
    )
    return parser.parse_args()


# Main
def main():
    """
    Entry point for the tool.
    """

    args = parse_args()

    clean_payload = args.payload.replace(":", "")
    payload_bytes = bytes.fromhex(clean_payload)

    if args.type in ["sent", "s"]:
        packet = PhoneCommandPacket(payload_bytes)
        logger.info(packet.pformat())

    if args.type in ["received", "r"]:
        packet = ReceivedPacket(payload_bytes)
        logger.info(packet.pformat())


if __name__ == "__main__":
    main()
