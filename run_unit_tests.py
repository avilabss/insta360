import unittest
import coverage
import sys


# Start code coverage
cov = coverage.Coverage()
cov.start()

# Run utility unit tests
utility_test_suite = unittest.TestLoader().discover("unit_tests/utility_tests")
unittest.TextTestRunner().run(utility_test_suite)

# Run OSC unit tests
osc_test_suite = unittest.TestLoader().discover("unit_tests/osc_tests")
unittest.TextTestRunner().run(osc_test_suite)

# Run RTMP unit tests
rtmp_test_suite = unittest.TestLoader().discover("unit_tests/rtmp_tests")
unittest.TextTestRunner().run(rtmp_test_suite)

# Stop code coverage
cov.stop()
cov.save()

# Generate code coverage report
try:
    print("Generating code coverage report...")
    coverage_percent = cov.report()
except coverage.CoverageException as e:
    sys.exit(f"Error generating code coverage report: {e}")

# Fail if total code coverage is less than 90%
print("Checking code coverage...")
if coverage_percent <= 90:
    sys.exit(f"Code coverage is {coverage_percent}%. Minimum required is 90%.")
else:
    print(f"Code coverage is {coverage_percent}%")

# Generate code coverage HTML report (optional)
# cov.html_report(directory='unit_tests_coverage_report')
