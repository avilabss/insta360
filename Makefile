.PHONY: unit-test integration-test test

unit-test:
	poetry run python3 run_unit_tests.py

integration-test:
	poetry run python3 run_integration_tests.py

test:
	make unit-test && make integration-test
