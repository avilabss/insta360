# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: authorization_operation_type.proto
# Protobuf Python Version: 5.26.1
"""Generated protocol buffer code."""

from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(
    b'\n"authorization_operation_type.proto\x12\x11insta360.messages*C\n\x1a\x41uthorizationOperationType\x12\x0f\n\x0b\x42LE_CONNECT\x10\x00\x12\x14\n\x10WIFI_PWD_SETTING\x10\x01\x42\x08\xa2\x02\x05INSPBb\x06proto3'
)

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(
    DESCRIPTOR, "authorization_operation_type_pb2", _globals
)
if not _descriptor._USE_C_DESCRIPTORS:
    _globals["DESCRIPTOR"]._loaded_options = None
    _globals["DESCRIPTOR"]._serialized_options = b"\242\002\005INSPB"
    _globals["_AUTHORIZATIONOPERATIONTYPE"]._serialized_start = 57
    _globals["_AUTHORIZATIONOPERATIONTYPE"]._serialized_end = 124
# @@protoc_insertion_point(module_scope)
