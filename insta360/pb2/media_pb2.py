# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: media.proto
# Protobuf Python Version: 5.26.1
"""Generated protocol buffer code."""

from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(
    b'\n\x0bmedia.proto\x12\x11insta360.messages"<\n\x03GPS\x12\x11\n\tlongitude\x18\x01 \x01(\x01\x12\x10\n\x08latitude\x18\x02 \x01(\x01\x12\x10\n\x08\x61ltitude\x18\x03 \x01(\x01"N\n\x04Gyro\x12\n\n\x02\x61x\x18\x01 \x01(\x01\x12\n\n\x02\x61y\x18\x02 \x01(\x01\x12\n\n\x02\x61z\x18\x03 \x01(\x01\x12\n\n\x02gx\x18\x04 \x01(\x01\x12\n\n\x02gy\x18\x05 \x01(\x01\x12\n\n\x02gz\x18\x06 \x01(\x01"9\n\x0bOrientation\x12\t\n\x01x\x18\x01 \x01(\x05\x12\t\n\x01y\x18\x02 \x01(\x05\x12\t\n\x01z\x18\x03 \x01(\x05\x12\t\n\x01w\x18\x04 \x01(\x05*\x1e\n\tAACBSType\x12\x07\n\x03RAW\x10\x00\x12\x08\n\x04\x41\x44TS\x10\x01*_\n\tMediaType\x12\t\n\x05VIDEO\x10\x00\x12\t\n\x05PHOTO\x10\x01\x12\x13\n\x0fVIDEO_AND_PHOTO\x10\x02\x12\x07\n\x03\x44NG\x10\x03\x12\x07\n\x03MP4\x10\x04\x12\x07\n\x03JPG\x10\x05\x12\x0c\n\x08INS_DATA\x10\x06\x42\x08\xa2\x02\x05INSPBb\x06proto3'
)

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, "media_pb2", _globals)
if not _descriptor._USE_C_DESCRIPTORS:
    _globals["DESCRIPTOR"]._loaded_options = None
    _globals["DESCRIPTOR"]._serialized_options = b"\242\002\005INSPB"
    _globals["_AACBSTYPE"]._serialized_start = 235
    _globals["_AACBSTYPE"]._serialized_end = 265
    _globals["_MEDIATYPE"]._serialized_start = 267
    _globals["_MEDIATYPE"]._serialized_end = 362
    _globals["_GPS"]._serialized_start = 34
    _globals["_GPS"]._serialized_end = 94
    _globals["_GYRO"]._serialized_start = 96
    _globals["_GYRO"]._serialized_end = 174
    _globals["_ORIENTATION"]._serialized_start = 176
    _globals["_ORIENTATION"]._serialized_end = 233
# @@protoc_insertion_point(module_scope)
