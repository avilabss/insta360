# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: flicker.proto
# Protobuf Python Version: 5.26.1
"""Generated protocol buffer code."""

from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(
    b"\n\rflicker.proto\x12\x11insta360.messages*?\n\x07\x46licker\x12\x10\n\x0c\x46LICKER_AUTO\x10\x00\x12\x10\n\x0c\x46LICKER_60HZ\x10\x01\x12\x10\n\x0c\x46LICKER_50HZ\x10\x02\x42\x08\xa2\x02\x05INSPBb\x06proto3"
)

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, "flicker_pb2", _globals)
if not _descriptor._USE_C_DESCRIPTORS:
    _globals["DESCRIPTOR"]._loaded_options = None
    _globals["DESCRIPTOR"]._serialized_options = b"\242\002\005INSPB"
    _globals["_FLICKER"]._serialized_start = 36
    _globals["_FLICKER"]._serialized_end = 99
# @@protoc_insertion_point(module_scope)
