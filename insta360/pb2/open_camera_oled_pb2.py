# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: open_camera_oled.proto
# Protobuf Python Version: 5.26.1
"""Generated protocol buffer code."""

from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(
    b'\n\x16open_camera_oled.proto\x12\x11insta360.messages"\x10\n\x0eOpenCameraOled"\x14\n\x12OpenCameraOledRespB\x08\xa2\x02\x05INSPBb\x06proto3'
)

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, "open_camera_oled_pb2", _globals)
if not _descriptor._USE_C_DESCRIPTORS:
    _globals["DESCRIPTOR"]._loaded_options = None
    _globals["DESCRIPTOR"]._serialized_options = b"\242\002\005INSPB"
    _globals["_OPENCAMERAOLED"]._serialized_start = 45
    _globals["_OPENCAMERAOLED"]._serialized_end = 61
    _globals["_OPENCAMERAOLEDRESP"]._serialized_start = 63
    _globals["_OPENCAMERAOLEDRESP"]._serialized_end = 83
# @@protoc_insertion_point(module_scope)
