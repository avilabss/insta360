# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: button_press_params.proto
# Protobuf Python Version: 5.26.1
"""Generated protocol buffer code."""

from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from .video_pb2 import *

DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(
    b'\n\x19\x62utton_press_params.proto\x12\x11insta360.messages\x1a\x0bvideo.proto"\x98\x01\n\x11\x42uttonPressParams\x12J\n\x1e\x62utton_param_record_resolution\x18\x01 \x01(\x0e\x32".insta360.messages.VideoResolution\x12\x1f\n\x17\x62utton_param_lapse_Time\x18\x02 \x01(\r\x12\x16\n\x0erec_limit_time\x18\x03 \x01(\r*\xaf\x01\n\x15\x42uttonPressParamsType\x12\x1f\n\x1b\x42UTTON_PRESS_PARAMS_UNKNOWN\x10\x00\x12"\n\x1e\x42UTTON_PARAM_RECORD_RESOLUTION\x10\x01\x12\x1b\n\x17\x42UTTON_PARAM_LAPSE_TIME\x10\x02\x12\x1e\n\x1a\x42UTTON_PARAM_RES_REC_LIMIT\x10\x03\x12\x14\n\x10\x42UTTON_PARAM_NUM\x10\x04\x42\x08\xa2\x02\x05INSPBP\x00\x62\x06proto3'
)

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, "button_press_params_pb2", _globals)
if not _descriptor._USE_C_DESCRIPTORS:
    _globals["DESCRIPTOR"]._loaded_options = None
    _globals["DESCRIPTOR"]._serialized_options = b"\242\002\005INSPB"
    _globals["_BUTTONPRESSPARAMSTYPE"]._serialized_start = 217
    _globals["_BUTTONPRESSPARAMSTYPE"]._serialized_end = 392
    _globals["_BUTTONPRESSPARAMS"]._serialized_start = 62
    _globals["_BUTTONPRESSPARAMS"]._serialized_end = 214
# @@protoc_insertion_point(module_scope)
