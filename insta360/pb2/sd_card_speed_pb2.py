# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: sd_card_speed.proto
# Protobuf Python Version: 5.26.1
"""Generated protocol buffer code."""

from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(
    b'\n\x13sd_card_speed.proto\x12\x11insta360.messages"F\n\x0fTestSDCardSpeed\x12\x12\n\nblock_size\x18\x01 \x01(\r\x12\x10\n\x08\x64uration\x18\x02 \x01(\r\x12\r\n\x05times\x18\x03 \x01(\r"+\n\x13TestSDCardSpeedResp\x12\x14\n\x0cwrite_speeds\x18\x01 \x03(\x01\x42\x08\xa2\x02\x05INSPBb\x06proto3'
)

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, "sd_card_speed_pb2", _globals)
if not _descriptor._USE_C_DESCRIPTORS:
    _globals["DESCRIPTOR"]._loaded_options = None
    _globals["DESCRIPTOR"]._serialized_options = b"\242\002\005INSPB"
    _globals["_TESTSDCARDSPEED"]._serialized_start = 42
    _globals["_TESTSDCARDSPEED"]._serialized_end = 112
    _globals["_TESTSDCARDSPEEDRESP"]._serialized_start = 114
    _globals["_TESTSDCARDSPEEDRESP"]._serialized_end = 157
# @@protoc_insertion_point(module_scope)
