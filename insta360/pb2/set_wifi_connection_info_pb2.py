# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: set_wifi_connection_info.proto
# Protobuf Python Version: 5.26.1
"""Generated protocol buffer code."""

from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from .wifi_connection_info_pb2 import *

DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(
    b'\n\x1eset_wifi_connection_info.proto\x12\x11insta360.messages\x1a\x1awifi_connection_info.proto"\\\n\x15SetWifiConnectionInfo\x12\x43\n\x14wifi_connection_info\x18\x01 \x01(\x0b\x32%.insta360.messages.WifiConnectionInfo"\x1b\n\x19SetWifiConnectionInfoRespB\x08\xa2\x02\x05INSPBP\x00\x62\x06proto3'
)

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(
    DESCRIPTOR, "set_wifi_connection_info_pb2", _globals
)
if not _descriptor._USE_C_DESCRIPTORS:
    _globals["DESCRIPTOR"]._loaded_options = None
    _globals["DESCRIPTOR"]._serialized_options = b"\242\002\005INSPB"
    _globals["_SETWIFICONNECTIONINFO"]._serialized_start = 81
    _globals["_SETWIFICONNECTIONINFO"]._serialized_end = 173
    _globals["_SETWIFICONNECTIONINFORESP"]._serialized_start = 175
    _globals["_SETWIFICONNECTIONINFORESP"]._serialized_end = 202
# @@protoc_insertion_point(module_scope)
