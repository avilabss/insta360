# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: set_multi_photography_options.proto
# Protobuf Python Version: 5.26.1
"""Generated protocol buffer code."""

from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from .options_pb2 import *
from .multi_photography_options_pb2 import *

DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(
    b'\n#set_multi_photography_options.proto\x12\x11insta360.messages\x1a\roptions.proto\x1a\x1fmulti_photography_options.proto"\x86\x02\n\x1aSetMultiPhotographyOptions\x12\x44\n\x0coption_types\x18\x01 \x03(\x0e\x32..insta360.messages.MultiPhotographyOptionsType\x12\x39\n\x05value\x18\x02 \x01(\x0b\x32*.insta360.messages.MultiPhotographyOptions\x12/\n\x06\x64\x65vice\x18\x03 \x01(\x0e\x32\x1f.insta360.messages.SensorDevice\x12\x36\n\rfunction_mode\x18\x04 \x01(\x0e\x32\x1f.insta360.messages.FunctionMode"g\n\x1eSetMultiPhotographyOptionsResp\x12\x45\n\rsuccess_types\x18\x01 \x03(\x0e\x32..insta360.messages.MultiPhotographyOptionsTypeB\x08\xa2\x02\x05INSPBP\x00P\x01\x62\x06proto3'
)

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(
    DESCRIPTOR, "set_multi_photography_options_pb2", _globals
)
if not _descriptor._USE_C_DESCRIPTORS:
    _globals["DESCRIPTOR"]._loaded_options = None
    _globals["DESCRIPTOR"]._serialized_options = b"\242\002\005INSPB"
    _globals["_SETMULTIPHOTOGRAPHYOPTIONS"]._serialized_start = 107
    _globals["_SETMULTIPHOTOGRAPHYOPTIONS"]._serialized_end = 369
    _globals["_SETMULTIPHOTOGRAPHYOPTIONSRESP"]._serialized_start = 371
    _globals["_SETMULTIPHOTOGRAPHYOPTIONSRESP"]._serialized_end = 474
# @@protoc_insertion_point(module_scope)
