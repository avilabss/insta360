# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: set_button_press_params.proto
# Protobuf Python Version: 5.26.1
"""Generated protocol buffer code."""

from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from .button_press_params_pb2 import *
from .button_press_pb2 import *

DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(
    b'\n\x1dset_button_press_params.proto\x12\x11insta360.messages\x1a\x19\x62utton_press_params.proto\x1a\x12\x62utton_press.proto"\x84\x02\n\x0fSetButtonParams\x12>\n\x0c\x62utton_param\x18\x01 \x03(\x0e\x32(.insta360.messages.ButtonPressParamsType\x12\x33\n\x05value\x18\x02 \x01(\x0b\x32$.insta360.messages.ButtonPressParams\x12=\n\x11\x62utton_press_type\x18\x03 \x01(\x0e\x32".insta360.messages.ButtonPressType\x12=\n\x11\x62utton_press_mode\x18\x04 \x01(\x0e\x32".insta360.messages.ButtonPressMode"V\n\x13SetButtonParamsResp\x12?\n\rsuccess_types\x18\x01 \x03(\x0e\x32(.insta360.messages.ButtonPressParamsTypeB\x08\xa2\x02\x05INSPBP\x00P\x01\x62\x06proto3'
)

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(
    DESCRIPTOR, "set_button_press_params_pb2", _globals
)
if not _descriptor._USE_C_DESCRIPTORS:
    _globals["DESCRIPTOR"]._loaded_options = None
    _globals["DESCRIPTOR"]._serialized_options = b"\242\002\005INSPB"
    _globals["_SETBUTTONPARAMS"]._serialized_start = 100
    _globals["_SETBUTTONPARAMS"]._serialized_end = 360
    _globals["_SETBUTTONPARAMSRESP"]._serialized_start = 362
    _globals["_SETBUTTONPARAMSRESP"]._serialized_end = 448
# @@protoc_insertion_point(module_scope)
