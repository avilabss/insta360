# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: get_file_finish.proto
# Protobuf Python Version: 5.26.1
"""Generated protocol buffer code."""

from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from .file_type_pb2 import *
from .track_pb2 import *

DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(
    b'\n\x15get_file_finish.proto\x12\x11insta360.messages\x1a\x0f\x66ile_type.proto\x1a\x0btrack.proto"t\n\rGetFileFinish\x12.\n\tfile_type\x18\x01 \x01(\x0e\x32\x1b.insta360.messages.FileType\x12\x33\n\x08\x64ownload\x18\x02 \x01(\x0e\x32!.insta360.messages.TransferStatusB\x08\xa2\x02\x05INSPBP\x00P\x01\x62\x06proto3'
)

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, "get_file_finish_pb2", _globals)
if not _descriptor._USE_C_DESCRIPTORS:
    _globals["DESCRIPTOR"]._loaded_options = None
    _globals["DESCRIPTOR"]._serialized_options = b"\242\002\005INSPB"
    _globals["_GETFILEFINISH"]._serialized_start = 74
    _globals["_GETFILEFINISH"]._serialized_end = 190
# @@protoc_insertion_point(module_scope)
