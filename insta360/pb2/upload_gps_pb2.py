# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: upload_gps.proto
# Protobuf Python Version: 5.26.1
"""Generated protocol buffer code."""

from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(
    b'\n\x10upload_gps.proto\x12\x11insta360.messages"\x18\n\tUploadGps\x12\x0b\n\x03gps\x18\x01 \x01(\x0c"\x0f\n\rUploadGpsRespB\x08\xa2\x02\x05INSPBb\x06proto3'
)

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, "upload_gps_pb2", _globals)
if not _descriptor._USE_C_DESCRIPTORS:
    _globals["DESCRIPTOR"]._loaded_options = None
    _globals["DESCRIPTOR"]._serialized_options = b"\242\002\005INSPB"
    _globals["_UPLOADGPS"]._serialized_start = 39
    _globals["_UPLOADGPS"]._serialized_end = 63
    _globals["_UPLOADGPSRESP"]._serialized_start = 65
    _globals["_UPLOADGPSRESP"]._serialized_end = 80
# @@protoc_insertion_point(module_scope)
