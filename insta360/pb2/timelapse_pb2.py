# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: timelapse.proto
# Protobuf Python Version: 5.26.1
"""Generated protocol buffer code."""

from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import symbol_database as _symbol_database
from google.protobuf.internal import builder as _builder
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(
    b'\n\x0ftimelapse.proto\x12\x11insta360.messages"\xcf\x01\n\x10TimelapseOptions\x12\x10\n\x08\x64uration\x18\x01 \x01(\r\x12\x11\n\tlapseTime\x18\x02 \x01(\r\x12\x43\n\x0boutput_type\x18\x03 \x01(\x0e\x32..insta360.messages.TimelapseOptions.OutputType\x12\x1b\n\x13\x61\x63\x63\x65lerate_fequency\x18\x04 \x01(\r"4\n\nOutputType\x12\x0f\n\x0b\x44oNotChange\x10\x00\x12\t\n\x05Video\x10\x01\x12\n\n\x06Images\x10\x02*\xc1\x02\n\rTimelapseMode\x12\x13\n\x0fTIMELAPSE_MIXED\x10\x00\x12\x1a\n\x16MOBILE_TIMELAPSE_VIDEO\x10\x01\x12\x1f\n\x1bTIMELAPSE_INTERVAL_SHOOTING\x10\x02\x12\x1a\n\x16STATIC_TIMELAPSE_VIDEO\x10\x03\x12\x1c\n\x18TIMELAPSE_INTERVAL_VIDEO\x10\x04\x12 \n\x1cTIMELAPSE_STARLAPSE_SHOOTING\x10\x05\x12\x1d\n\x19TIMELAPSE_QC_SINGLE_PRESS\x10\x06\x12\x1d\n\x19TIMELAPSE_QC_DOUBLE_PRESS\x10\x07\x12!\n\x1dTIMELAPSE_CAMERA_SINGLE_PRESS\x10\x08\x12!\n\x1dTIMELAPSE_CAMERA_DOUBLE_PRESS\x10\tB\x08\xa2\x02\x05INSPBb\x06proto3'
)

_globals = globals()
_builder.BuildMessageAndEnumDescriptors(DESCRIPTOR, _globals)
_builder.BuildTopDescriptorsAndMessages(DESCRIPTOR, "timelapse_pb2", _globals)
if not _descriptor._USE_C_DESCRIPTORS:
    _globals["DESCRIPTOR"]._loaded_options = None
    _globals["DESCRIPTOR"]._serialized_options = b"\242\002\005INSPB"
    _globals["_TIMELAPSEMODE"]._serialized_start = 249
    _globals["_TIMELAPSEMODE"]._serialized_end = 570
    _globals["_TIMELAPSEOPTIONS"]._serialized_start = 39
    _globals["_TIMELAPSEOPTIONS"]._serialized_end = 246
    _globals["_TIMELAPSEOPTIONS_OUTPUTTYPE"]._serialized_start = 194
    _globals["_TIMELAPSEOPTIONS_OUTPUTTYPE"]._serialized_end = 246
# @@protoc_insertion_point(module_scope)
