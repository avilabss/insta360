"""
This is a command line tool to control and interact with Insta360 camera using the rtmp protocol.

Example:
    ```bash
    # Start preview stream and capture video
    python3 ./insta360_cmd.py -c preview -c capture -o ./output/dump.mp4

    # Start livestream
    python3 ./insta360_cmd.py -c livestream -o ./output/dump.mp4

    # Capture video
    python3 ./insta360_cmd.py -c capture
    ```
"""

import argparse
import logging

from insta360.rtmp import Client


# Setup logging
logger = logging.getLogger("insta360_cmd")
handler = logging.StreamHandler()
formatter = logging.Formatter(
    "%(asctime)s: %(levelname)s: %(name)s: %(message)s", datefmt="%Y-%m-%d %H:%M:%S"
)
handler.setFormatter(formatter)
logger.addHandler(handler)


def handle_camera_commands(args: argparse.Namespace):
    """
    Handler for executing camera commands.

    Parameters:
        commands: List of commands to execute.
        output_filepath: Output file path for saving the video stream.
    """

    commands = args.command
    output_filepath = args.output

    logger.info("Connecting to camera")
    client = Client(
        host="192.168.42.1",
        port=6666,
        output_filepath=output_filepath,
        enable_hwaccel=not args.no_hwaccel,
    )
    client.open()

    command_order = ["livestream", "preview", "capture"]
    commands = sorted(commands, key=lambda x: command_order.index(x))

    for command in commands:
        if command == "livestream":
            logger.info("Starting livestream")
            client.start_live_stream()

        elif command == "preview":
            logger.info("Starting preview stream")
            client.start_preview_stream()

        elif command == "capture":
            logger.info("Capturing video")
            client.start_capture()

    logger.info("Commands executed successfully")
    logger.info("Press Ctrl+C to stop the commands and close the connection")

    if args.display:
        client.display_stream()


def parse_args():
    """
    Parse command line arguments.

    Returns:
        Parsed arguments.
    """

    parser = argparse.ArgumentParser(
        prog="insta360_cmd",
        description="Control & Interact with Insta360 from command line",
    )

    parser.add_argument(
        "-c",
        "--command",
        required=True,
        action="append",
        choices=["livestream", "preview", "capture"],
        help="Commands to execute",
    )
    parser.add_argument("-o", "--output", help="Output file")
    parser.add_argument("-v", "--verbose", action="store_true", help="Verbose output")
    parser.add_argument(
        "-d", "--display", action="store_true", help="Display video stream"
    )
    parser.add_argument(
        "--no-hwaccel", action="store_true", help="Disable hardware acceleration"
    )

    return parser.parse_args()


def main():
    """
    Entry point for the command line tool.
    """

    args = parse_args()

    if args.verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    if "livestream" in args.command and "preview" in args.command:
        raise ValueError("Cannot start livestream and preview at the same time")

    handle_camera_commands(args)


if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        logger.error(e)
